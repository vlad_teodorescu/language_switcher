#!/bin/sh
#
# Prepare the project for compilation ...
#

set -e

# if get an error about libtool not setup
# " error: Libtool library used but 'LIBTOOL' is undefined
#     The usual way to define 'LIBTOOL' is to add 'LT_INIT' "
# manually call libtoolize or glibtoolize before running this again
# (g)libtoolize

# if you get an error about config.rpath missing, some buggy automake versions
# then touch the missing file (may need to make config/ first).
# touch config/config.rpath
# touch config.rpath


# If this is a source checkout then call autoreconf with error as well
if test -d .git; then
	WARNINGS="all,error"
else
	WARNINGS="all"
fi

autoreconf --install --force --verbose

#where:
# --force rebuilds ./configure regardless
# --install copies some missing files to the directory including txt files

# autoreconf also installs some files that we do not need
rm -f po/Makevars.template
rm -f po/Rules-quot
rm -f po/boldquot.sed
rm -f po/en@boldquot.header
rm -f po/en@quot.header
rm -f po/insert-header.sin
rm -f po/quot.sed

echo "po/ folder cleaned"

# without an existing po/POTFILES.in file, the Makefile for
# the po/ folder will not generated, which leads to failure to make
po/refresh_potfiles_in.sh ./src
