/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "MenuBuilder.h"

#include <spdlog/spdlog.h>

MenuBuilder::MenuBuilder(const string &gid_actiongroup) : gid_actiongroup(gid_actiongroup)
{
}

void MenuBuilder::addResourceSetEntries(LocalizedResourceSet &resource_set, const sigc::slot<void(string)> &action)
{
    for (LocalizedResourceSet::const_iterator it = resource_set.begin(); it != resource_set.end(); it++)
    {
        string code = "select_lang_" + it->getCode();
        shared_ptr<MenuBuilderEntryRadio> entry = make_shared<MenuBuilderEntryRadio>();
        entry->action = action;
        entry->target = it->getCode();

        this->populateMenuEntryData(entry, 0, code, it->getName(), it->getIconPath());
        this->menu_entries.push_back(entry);
    }
}

void MenuBuilder::addCustomEntry(const string title, const Gtk::BuiltinStockID &stock_icon, const sigc::slot<void(void)> &action)
{
    this->addCustomEntry(title, stock_icon.id, action);
}

void MenuBuilder::addCustomEntry(const string title, const string icon, const sigc::slot<void(void)> &action)
{
    string code = "custom_action_" + to_string(this->action_index++);
    shared_ptr<MenuBuilderEntryStandard> entry = make_shared<MenuBuilderEntryStandard>();
    entry->action = action;
    this->populateMenuEntryData(entry, 1, code, title, icon);
    this->menu_entries.push_back(entry);
}

void MenuBuilder::populateMenuEntryData(shared_ptr<MenuBuilderEntryBase> entry, int section_id, string code, string title, string icon) const
{
    entry->section_id = section_id;
    entry->code = code;
    entry->title = title;
    entry->gid_actiongroup = this->gid_actiongroup;
    if (!icon.empty())
        entry->icon = Gio::Icon::create(icon);
}

void MenuBuilder::clearAllEntries()
{
    this->menu_entries.clear();
    this->action_index = 0;
}

void MenuBuilder::populateMenuEntries(Glib::RefPtr<Gio::Menu> &menu) const
{
    int last_section_id = -1;
    Glib::RefPtr<Gio::Menu> section;
    for (vector<shared_ptr<MenuBuilderEntryBase>>::const_iterator it = this->menu_entries.begin(); it != this->menu_entries.end(); it++)
    {
        if ((*it)->section_id != last_section_id)
        {
            last_section_id = (*it)->section_id;
            if (section)
                menu->append_section(section);

            section = Gio::Menu::create();
        }

        section->append_item((*it)->createMenuItem());
    }

    if (section)
        menu->append_section(section);
}

void MenuBuilder::populateActions(Glib::RefPtr<Gio::SimpleActionGroup> &action_group, const string &current_state) const
{
    for (vector<shared_ptr<MenuBuilderEntryBase>>::const_iterator it = this->menu_entries.begin(); it != this->menu_entries.end(); it++)
    {
        spdlog::info("Populating action {}", (*it)->code);
        action_group->add_action((*it)->createAction(current_state));

        continue;
        // todo: reselect proper layout based on code
        Glib::RefPtr<Gio::Action> found_act = action_group->lookup_action((*it)->code);

        if (found_act)
            found_act->change_state(Glib::Variant<Glib::ustring>::create("modified_initial_state"));

        Glib::ustring out;
        action_group->get_action_state((*it)->code, out);

        spdlog::info("Action state is {}", out.c_str());
    }
}

string MenuBuilder::getActionGroupId() const
{
    return this->gid_actiongroup;
}
