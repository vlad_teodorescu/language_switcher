/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "Systray.h"

#include <spdlog/spdlog.h>

#include "../../i18n/i18n.h"

Systray::Systray(string icon_filepath, MenuBuilder &menu_builder) : menu_builder(menu_builder)
{
    this->action_group = Gio::SimpleActionGroup::create();

    this->status_icon = Gtk::StatusIcon::create_from_file(icon_filepath);
    this->status_icon->signal_popup_menu().connect(sigc::mem_fun(*this, &Systray::onStatusIconPopup));

    this->menu_model = Gio::Menu::create();
    menu_builder.populateMenuEntries(this->menu_model);

    this->context_menu = std::make_unique<Gtk::Menu>(this->menu_model);
    this->context_menu->set_tooltip_text(_("menu.name"));
    this->context_menu->insert_action_group(this->menu_builder.getActionGroupId(), this->action_group);
}

Glib::RefPtr<Gio::SimpleActionGroup>& Systray::getActionGroup()
{
    return this->action_group;
}

void Systray::setIcon(string icon)
{
    this->status_icon->set_from_file(icon);
}

void Systray::onStatusIconPopup(guint button, guint32 activate_time)
{
    this->context_menu->popup(button, activate_time);
}

void Systray::setTooltip(string text)
{
    this->status_icon->set_tooltip_text(text);
}