/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef SYSTRAY_H
#define SYSTRAY_H
#pragma once

#include <gtkmm.h>

#include "MenuBuilder.h"
#include "config.h"

class Systray
{
private:
    static inline const string CFG_LAYOUT_NAME = "layout.name";

    Systray(const Systray &that) = delete;
    Systray &operator=(const Systray &that) = delete;

    MenuBuilder &menu_builder;

    // Controls the system tray icon
    Glib::RefPtr<Gtk::StatusIcon> status_icon;
    Glib::RefPtr<Gio::Menu> menu_model;
    std::unique_ptr<Gtk::Menu> context_menu;
    Glib::RefPtr<Gio::SimpleActionGroup> action_group;

protected:
    /**
     * @brief Performed when the user right-clicks the status icon.
     *
     * @param button button id
     * @param activate_time activation time
     * @see Gtk::Menu::popup()
     */
    void onStatusIconPopup(guint button, guint32 activate_time);

public:
    /**
     * @brief Constructs a new Systray object.
     *
     * @param icon_filepath path to the icon which will be displayed in the system tray
     * @param menu_builder builder class that will populate the menu entries
     */
    Systray(string icon_filepath, MenuBuilder &menu_builder);

    /**
     * @brief Set the icon displayed in the system tray.
     *
     * @param icon path to icon
     */
    void setIcon(string icon);

    /**
     * @brief Set the tooltip text displayed when the user hovers over the system tray icon.
     *
     * @param text tooltip text
     */
    void setTooltip(string text);

    Glib::RefPtr<Gio::SimpleActionGroup>& getActionGroup();
};

#endif // SYSTRAY_H