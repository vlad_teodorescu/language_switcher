/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "MenuBuilderEntry.h"

#include <spdlog/spdlog.h>

namespace
{
    // Straight out of glibmm/actionmap.ccg:
    // Handle the normal activate signal, calling instead a slot that takes the specific type:
    static void on_action_radio_string(const Glib::VariantBase &parameter, const Gio::ActionMap::ActivateWithStringParameterSlot &slot)
    {
        const auto variantDerived = Glib::VariantBase::cast_dynamic<Glib::Variant<Glib::ustring>>(parameter);
        const auto str = variantDerived.get();
        slot(str);
    }

} // anonymous namespace

Glib::RefPtr<Gio::MenuItem> MenuBuilderEntryBase::createMenuItem() const
{
    // spdlog::debug("MenuBuilderEntryBase::createMenuItem({})", gid_actiongroup);
    Glib::ustring action_code = Glib::ustring(this->gid_actiongroup + "." + this->code);
    Glib::RefPtr<Gio::MenuItem> menu_item = Gio::MenuItem::create(this->title, action_code);
    menu_item->set_icon(this->icon);

    return menu_item;
}

Glib::RefPtr<Gio::SimpleAction> MenuBuilderEntryStandard::createAction(const string &current_state) const
{
    Glib::RefPtr<Gio::SimpleAction> act = Gio::SimpleAction::create(this->code);
    act->signal_activate().connect(sigc::hide(this->action));

    return act;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Glib::RefPtr<Gio::MenuItem> MenuBuilderEntryRadio::createMenuItem() const
{
    Glib::RefPtr<Gio::MenuItem> menu_item = MenuBuilderEntryBase::createMenuItem();
    menu_item->set_attribute("target", Glib::Variant<Glib::ustring>::create(this->target));

    return menu_item;
}

Glib::RefPtr<Gio::SimpleAction> MenuBuilderEntryRadio::createAction(const string &current_state) const
{
    Glib::RefPtr<Gio::SimpleAction> act = Gio::SimpleAction::create_radio_string(this->code, current_state);
    act->signal_activate().connect(sigc::bind(sigc::ptr_fun(&on_action_radio_string), this->action));

    return act;
}