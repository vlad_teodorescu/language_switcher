/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef MENUBUILDERENTRY_H
#define MENUBUILDERENTRY_H
#pragma once

#include <gtkmm.h>
#include <string>

using namespace std;

class MenuBuilder;

class MenuBuilderEntryBase
{
    friend MenuBuilder;

protected:
    int section_id;
    string code;
    string title;
    string gid_actiongroup;
    Glib::RefPtr<Gio::Icon> icon;

    /**
     * @brief Creates a MenuItem object based on the data stored in this struct.
     *
     * @return pointer to a MenuItem
     */
    virtual Glib::RefPtr<Gio::MenuItem> createMenuItem() const;

    /**
     * @brief Creates a SimpleAction object based on the data stored in this struct.
     * @param current_state current state of the action (relevant for radio buttons)
     *
     * @return pointer to a SimpleAction
     */
    virtual Glib::RefPtr<Gio::SimpleAction> createAction(const string &current_state) const = 0;

    virtual ~MenuBuilderEntryBase() {};
};

class MenuBuilderEntryStandard : public MenuBuilderEntryBase
{
    friend MenuBuilder;

protected:
    sigc::slot<void(void)> action;

    /**
     * @see MenuBuilderEntryBase::createAction().
     */
    Glib::RefPtr<Gio::SimpleAction> createAction(const string &current_state) const;
};

class MenuBuilderEntryRadio : public MenuBuilderEntryBase
{
    friend MenuBuilder;

protected:
    string target;
    sigc::slot<void(string)> action;

    /**
     * @see MenuBuilderEntryBase::createMenuItem().
     */
    Glib::RefPtr<Gio::MenuItem> createMenuItem() const;

    /**
     * @see MenuBuilderEntryBase::createAction().
     */
    Glib::RefPtr<Gio::SimpleAction> createAction(const string &current_state) const;
};

#endif // MENUBUILDERENTRY_H