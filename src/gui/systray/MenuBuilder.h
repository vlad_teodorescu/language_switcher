/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef MENUBUILDER_H
#define MENUBUILDER_H
#pragma once

#include <gtkmm.h>
#include <sigc++/slot.h>
#include <string>
#include <vector>

#include "../../resources/LocalizedResourceSet.h"
#include "MenuBuilderEntry.h"

using namespace std;

class MenuBuilder
{
private:
    MenuBuilder(const MenuBuilder &that) = delete;
    MenuBuilder &operator=(const MenuBuilder &that) = delete;

    // vector stores pointers because the base class is abstract
    vector<shared_ptr<MenuBuilderEntryBase>> menu_entries;
    int action_index = 0;
    const string gid_actiongroup;

protected:
    /**
     * @brief Populates a menu entry with the given element data
     *
     * @param entry menu entry
     * @param section_id ID of section to which the menu belongs
     * @param code entry code
     * @param title entry title
     * @param icon path to entry icon
     */
    void populateMenuEntryData(shared_ptr<MenuBuilderEntryBase> entry, int section_id, string code, string title, string icon) const;

public:
    /**
     * @brief Constructs a new MenuBuilder object
     *
     * @param gid_actiongroup group ID to which the entries belong (related to the actions that the menu entry will perform)
     */
    MenuBuilder(const string &gid_actiongroup);

    /**
     * @brief Adds menu entries based on the entries in the given resource set. Each resource entry will get its own menu entry.
     *
     * @param resource_set resource set to iterate through
     * @param action action to perform when the generated menu entry is clicked (resource code is passed as a string parameter)
     */
    void addResourceSetEntries(LocalizedResourceSet &resource_set, const sigc::slot<void(string)> &action);

    /**
     * @brief Adds a custom menu entry.
     *
     * @param title menu entry title
     * @param stock_icon stock icon (if one of GTK's stock icons is to be used)
     * @param action action to be performed when the menu entry is clicked
     */
    void addCustomEntry(const string title, const Gtk::BuiltinStockID &stock_icon, const sigc::slot<void(void)> &action);

    /**
     * @brief Adds a custom menu entry.
     *
     * @param title menu entry title
     * @param icon path to icon file
     * @param action action to be performed when the menu entry is clicked
     */
    void addCustomEntry(const string title, const string icon, const sigc::slot<void(void)> &action);

    /**
     * @brief Clears all existing menu entries.
     */
    void clearAllEntries();

    /**
     * @brief Populates the menu entries into the given menu.
     *
     * @param menu container object into which the entries will be created
     */
    void populateMenuEntries(Glib::RefPtr<Gio::Menu> &menu) const;

    /**
     * @brief Populates the menu entry actions into the given action group.
     *
     * @param action_group container object into which the actions will be created
     * @param current_state current state of the menu entry actions (relevant only for radio buttons)
     */
    void populateActions(Glib::RefPtr<Gio::SimpleActionGroup> &action_group, const string &current_state) const;

    /**
     * @brief Returns the ID of the action group for which this menu builder was configured.
     * 
     * @return action group ID
     */
    string getActionGroupId() const;
};

#endif // MENUBUILDER_H