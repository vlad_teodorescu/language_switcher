/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef PREFERENCESWINDOW_H
#define PREFERENCESWINDOW_H
#pragma once

#include <gtkmm.h>

#include "../../config/ApplicationConfig.h"
#include "../../resources/LocalizedResourceSet.h"
#include "../../shortcuts/ShortcutRegistrar.h"
#include "../components/ShortcutGrid.h"
#include "config.h"

using namespace std;

class LayoutsColumnRecord : public Gtk::TreeModel::ColumnRecord
{
public:
    // These types must match those for the model in the .glade file
    Gtk::TreeModelColumn<Glib::ustring> code;
    Gtk::TreeModelColumn<Glib::ustring> label;

    // This order must match the column order in the .glade file
    LayoutsColumnRecord()
    {
        add(code);
        add(label);
    }
};

class PreferencesWindow : public Gtk::Window
{
public:
    /**
     * @brief Default constructor.
     */
    PreferencesWindow(ShortcutRegistrar &registrar, ApplicationConfig &config, LocalizedResourceSet &resource_set);

    /**
     * @brief Default destructor.
     */
    ~PreferencesWindow();

protected:
    static inline const string DATA_DIR = PACKAGE_DATA_DIR;
    static inline const string BLUEPRINT_FILE_NAME = "preferences_window.glade";

    /**
     * @brief Returns the file path of the file containing the Glade blueprint for the preferences window.
     * 
     * @return string blueprint file path
     */
    string getGladeBlueprintFilePath();

    /**
     * @brief Configures the preferences window.
     */
    void configureWindow();

    /**
     * @brief Configures the ListStore object for the layout selector.
     */
    void configureListStoreLayouts();

    /**
     * @brief Configures the control (cancel/save) buttons.
     */
    void configureControlButtons();

    /**
     * @brief Configures the default layout selector widget.
     */
    void configureDefaultLayoutSelector();

    /**
     * @brief Configures the "Show notifications on layout change" checkbox.
     */
    void configureNotificationsCheckbox();

    /**
     * @brief Configure label text and translation for labels which are direct children of the window itself.
     */
    void configureLabels();

    /**
     * @brief Configures an individual child label.
     *
     * @param widget_id widget ID of the label displayed in the given cell
     * @param text the text that should be displayed in the given cell
     * @param tooltip the tooltip that should be displayed (on mouseover) for the given cell
     */
    void configureLabel(const string &widget_id, const string &text, const string &tooltip);

    /**
     * @brief Saves the preferences to disk and applies them to the running system.
     */
    void save();

    /**
     * @brief Executed before the window is shown.
     */
    void beforeShow();

    /**
     * @brief Executed after the window gets hidden.
     */
    void afterHide();

    /**
     * @brief Reads the config and selects the default layout to be displayed in the layout selector drop-down.
     */
    void selectDefaultLayoutFromConfig();

private:
    Glib::RefPtr<Gtk::Builder> builder;
    ShortcutRegistrar &registrar;
    ApplicationConfig &config;
    LocalizedResourceSet &resource_set;
    ShortcutGrid shortcut_grid;
};

#endif