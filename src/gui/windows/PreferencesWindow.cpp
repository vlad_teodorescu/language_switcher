/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "PreferencesWindow.h"

#include "../../exceptions/SystemException.h"
#include "../../i18n/i18n.h"
#include "../../utils/gtk.h"
#include "../../utils/path.h"
#include <spdlog/spdlog.h>

PreferencesWindow::PreferencesWindow(ShortcutRegistrar &registrar, ApplicationConfig &config, LocalizedResourceSet &resource_set)
    : builder(Gtk::Builder::create_from_file(getGladeBlueprintFilePath())), registrar(registrar), config(config), resource_set(resource_set),
      shortcut_grid(builder, registrar)
{
    this->configureWindow();

    this->configureListStoreLayouts();
    this->configureDefaultLayoutSelector();
    this->configureNotificationsCheckbox();

    shortcut_grid.configureGrid();

    this->configureControlButtons();
    this->configureLabels();
}

void PreferencesWindow::configureWindow()
{
    Gtk::Window *main_window;
    builder->get_widget("window", main_window);
    assert(main_window);

    utils::gtk::copy_window_data(main_window, this);
    this->set_title(_("windows.preferences.title"));

    this->signal_show().connect(sigc::mem_fun(this, &PreferencesWindow::beforeShow));
    this->signal_hide().connect(sigc::mem_fun(this, &PreferencesWindow::afterHide));

    // without this, it would not be possible to move the top-level container to the PreferencesWindow object
    main_window->remove();

    Gtk::Container *container;
    builder->get_widget("main_container", container);
    assert(container);
    this->add(*container);
}

string PreferencesWindow::getGladeBlueprintFilePath()
{
    string ret = utils::path::merge({DATA_DIR, BLUEPRINT_FILE_NAME});

    return utils::path::expand_path(ret);
}

void PreferencesWindow::configureListStoreLayouts()
{
    LayoutsColumnRecord col;
    Glib::RefPtr<Gtk::ListStore> layouts = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(builder->get_object("layouts"));

    layouts->clear();
    for (auto it = this->resource_set.begin(); it != this->resource_set.end(); it++)
    {
        Gtk::TreeModel::Row row = *(layouts->append());
        row[col.code] = it->getCode();
        row[col.label] = it->getName();
    }
}

void PreferencesWindow::configureControlButtons()
{
    Glib::RefPtr<Gtk::Button> butt_cancel = Glib::RefPtr<Gtk::Button>::cast_dynamic(builder->get_object("window.controls.cancel"));
    butt_cancel->signal_clicked().connect(sigc::mem_fun(this, &PreferencesWindow::hide));
    butt_cancel->set_label(_("buttons.preferences.cancel"));

    Glib::RefPtr<Gtk::Button> butt_save = Glib::RefPtr<Gtk::Button>::cast_dynamic(builder->get_object("window.controls.save"));
    butt_save->signal_clicked().connect(sigc::mem_fun(this, &PreferencesWindow::save));
    butt_save->set_label(_("buttons.preferences.save"));
}

void PreferencesWindow::configureDefaultLayoutSelector()
{
    LayoutsColumnRecord col;

    Glib::RefPtr<Gtk::ComboBox> layout_control = Glib::RefPtr<Gtk::ComboBox>::cast_dynamic(builder->get_object("settings.default_layout"));
    layout_control->pack_start(col.label, Gtk::PackOptions::PACK_EXPAND_WIDGET);
}

void PreferencesWindow::configureNotificationsCheckbox()
{
    Glib::RefPtr<Gtk::CheckButton> notif_control = Glib::RefPtr<Gtk::CheckButton>::cast_dynamic(builder->get_object("settings.show_notification"));
    notif_control->set_active(this->config.getShowNotificationLayoutChange());
}

void PreferencesWindow::configureLabels()
{
    this->configureLabel("labels.default_layout", _("labels.preferences.default_layout.text"), _("labels.preferences.default_layout.tooltip"));
    this->configureLabel("labels.show_notification", _("labels.preferences.show_notification.text"), _("labels.preferences.show_notification.tooltip"));
}

void PreferencesWindow::configureLabel(const string &widget_id, const string &text, const string &tooltip)
{
    Gtk::Label *label;
    builder->get_widget(widget_id, label);
    assert(label);

    label->set_text(text);
    label->set_tooltip_text(tooltip);
}

void PreferencesWindow::save()
{
    spdlog::info("Saving preferences");

    // write default layout
    LayoutsColumnRecord col;
    Glib::RefPtr<Gtk::ComboBox> layout_control = Glib::RefPtr<Gtk::ComboBox>::cast_dynamic(builder->get_object("settings.default_layout"));
    Glib::ustring act = (*(layout_control->get_active()))[col.code];
    this->config.setDefaultLayout(act.c_str());

    // write notification
    Glib::RefPtr<Gtk::CheckButton> notif_control = Glib::RefPtr<Gtk::CheckButton>::cast_dynamic(builder->get_object("settings.show_notification"));
    this->config.setShowNotificationLayoutChange(notif_control->get_active());

    // write shortcuts
    this->registrar.mergeDraftShortcuts();

    this->config.save();
    this->hide();
}

void PreferencesWindow::beforeShow()
{
    this->selectDefaultLayoutFromConfig();

    // make sure draft shortcuts are initialized
    this->registrar.getDraftShortcuts();
}

void PreferencesWindow::afterHide()
{
    // discard drafts and everything else
    registrar.discardDraftShortcuts();
}

void PreferencesWindow::selectDefaultLayoutFromConfig()
{
    LayoutsColumnRecord col;

    Glib::RefPtr<Gtk::ComboBox> layout_control = Glib::RefPtr<Gtk::ComboBox>::cast_dynamic(builder->get_object("settings.default_layout"));
    string layout = this->config.getDefaultLayout();
    if (!layout.length())
    {
        layout_control->set_active(0);
        return;
    }

    Glib::RefPtr<Gtk::ListStore> layouts = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(builder->get_object("layouts"));
    unsigned int index = 0;
    for (auto it = layouts->children().begin(); it != layouts->children().end(); it++)
    {
        if ((*it)[col.code] == layout)
        {
            layout_control->set_active(index);
            return;
        }
        index++;
    }
}

PreferencesWindow::~PreferencesWindow()
{
}