/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef SHORTCUTGRIDROW_H
#define SHORTCUTGRIDROW_H
#pragma once

#include "../../shortcuts/Shortcut.h"
#include "../../shortcuts/ShortcutId.h"
#include <gtkmm.h>

using namespace std;

class ShortcutGridRow
{
public:
    /**
     * @brief Default constructor.
     */
    ShortcutGridRow(const ShortcutId &shortcut_id, Shortcut &shortcut);

    void attachTo(Glib::RefPtr<Gtk::Grid> &container, int index) const;

    /**
     * @brief Default destructor.
     */
    ~ShortcutGridRow();

protected:
    /**
     * @brief Configure the row's label (first column).
     *
     * @param label GTK label to configure
     */
    void configureLabel(shared_ptr<Gtk::Label> &label);

    /**
     * @brief Configure a shortcutcut entry (either main or alt) for the row. The entry to configure is identical, and the two entries
     * are only differentiated through the getter/setter parameters.
     *
     * @param entry GTK entry to configure
     * @param shortcutGet getter function of the shortcut (either main or alt)
     * @param shortcutSet setter function of the shortcut (either main or alt)
     */
    void configureEntry(shared_ptr<Gtk::Entry> &entry, sigc::slot<string(void)> shortcutGet, sigc::slot<void(string)> shortcutSet);

    /**
     * @brief Performs basic widget configuration, which is applicable to all 3 widgets (label and both entries).
     *
     * @param widget widget to configure
     */
    void configureWidget(Gtk::Widget &widget);

private:
    Shortcut &shortcut;
    const ShortcutId &shortcut_id;
    shared_ptr<Gtk::Label> row_label;
    shared_ptr<Gtk::Entry> row_main;
    shared_ptr<Gtk::Entry> row_alt;
};

#endif // SHORTCUTGRIDROW_H