/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "ShortcutGrid.h"
#include "../../i18n/i18n.h"
#include <assert.h>

ShortcutGrid::ShortcutGrid(Glib::RefPtr<Gtk::Builder> &builder, ShortcutRegistrar &registrar) : builder(builder), registrar(registrar)
{
    this->configureHeader();
}

ShortcutGrid::~ShortcutGrid()
{
}

void ShortcutGrid::configureGrid()
{
    this->clearEntries();

    Glib::RefPtr<Gtk::ListStore> shortcuts = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(builder->get_object("shortcuts"));
    Glib::RefPtr<Gtk::Grid> container = this->getContainer();

    ShortcutGroup &draftGroup = registrar.getDraftShortcuts();
    int row_index = 1;
    for (auto it = this->registrar.beginActions(); it != this->registrar.endActions(); it++)
    {
        const ShortcutId &shortcut_id = it->first.get();
        Shortcut &shortcut = draftGroup.lookup(shortcut_id);
        shared_ptr<ShortcutGridRow> row = make_shared<ShortcutGridRow>(shortcut_id, shortcut);
        row->attachTo(container, row_index);
        rows.push_back(row);

        row_index++;
    }
}

void ShortcutGrid::clearEntries()
{
    Glib::RefPtr<Gtk::Grid> container = this->getContainer();
    int num_rows = this->calcNumRows();

    // we stop at 1 because the first row contains the header
    for (int i = num_rows; i >= 1; i--)
        container->remove_row(i);

    rows.clear();
}

void ShortcutGrid::configureHeader()
{
    this->configureHeaderCell("labels.keybindings.title", _("labels.preferences.keybindings.title.text"), "");
    this->configureHeaderCell("labels.keybindings.main", _("labels.preferences.keybindings.main.text"),
                              _("labels.preferences.keybindings.main.tooltip"));
    this->configureHeaderCell("labels.keybindings.alt", _("labels.preferences.keybindings.alt.text"),
                              _("labels.preferences.keybindings.alt.tooltip"));
}

void ShortcutGrid::configureHeaderCell(const string &widget_id, const string &text, const string &tooltip)
{
    Gtk::Label *label;
    builder->get_widget(widget_id, label);
    assert(label);

    label->set_text(text);
    label->set_tooltip_text(tooltip);
}

int ShortcutGrid::calcNumRows()
{
    Glib::RefPtr<Gtk::Grid> container = this->getContainer();

    int num_children = container->get_children().size();
    int num_rows = num_children / NUM_COLUMNS;
    if (num_children % NUM_COLUMNS)
        num_rows++;

    return num_rows;
}

Glib::RefPtr<Gtk::Grid> ShortcutGrid::getContainer()
{
    return Glib::RefPtr<Gtk::Grid>::cast_dynamic(builder->get_object("shortcuts_container"));
}