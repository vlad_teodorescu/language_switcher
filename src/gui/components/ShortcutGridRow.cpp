/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "ShortcutGridRow.h"
#include "../../i18n/i18n.h"
#include <spdlog/spdlog.h>

namespace
{
    void onEntryIconActivated(Gtk::EntryIconPosition pos, const GdkEventButton *event, shared_ptr<Gtk::Entry> &control)
    {
        // left-click on secondary entry icon
        if (pos == Gtk::EntryIconPosition::ENTRY_ICON_SECONDARY && event->button == 1)
            control->set_text("");
    }

    void onEntryChanged(shared_ptr<Gtk::Entry> &control, sigc::slot<void(string)> shortcutSet)
    {
        string txt = control->get_text();
        shortcutSet(txt);
    }
} // namespace

ShortcutGridRow::ShortcutGridRow(const ShortcutId &shortcut_id, Shortcut &shortcut) : shortcut_id(shortcut_id), shortcut(shortcut)
{
    row_label = make_shared<Gtk::Label>();
    this->configureLabel(row_label);

    row_main = make_shared<Gtk::Entry>();
    this->configureEntry(row_main, sigc::mem_fun(&shortcut, &Shortcut::getMain), sigc::mem_fun(&shortcut, &Shortcut::setMain));

    row_alt = make_shared<Gtk::Entry>();
    this->configureEntry(row_alt, sigc::mem_fun(&shortcut, &Shortcut::getAlt), sigc::mem_fun(&shortcut, &Shortcut::setAlt));
}

void ShortcutGridRow::configureLabel(shared_ptr<Gtk::Label> &label)
{
    this->configureWidget(*label);

    label->set_halign(Gtk::Align::ALIGN_START);
    label->set_justify(Gtk::JUSTIFY_FILL);
    label->set_can_focus(true);
    label->set_text(shortcut_id.getName());
    label->set_tooltip_text(shortcut_id.getDescription());
}

void ShortcutGridRow::configureEntry(shared_ptr<Gtk::Entry> &entry, sigc::slot<string(void)> shortcutGet, sigc::slot<void(string)> shortcutSet)
{
    this->configureWidget(*entry);

    entry->set_can_focus(true);
    entry->set_icon_from_stock(Gtk::Stock::CLEAR, Gtk::ENTRY_ICON_SECONDARY);
    entry->set_icon_activatable(true, Gtk::ENTRY_ICON_SECONDARY);
    entry->set_icon_sensitive(Gtk::ENTRY_ICON_SECONDARY, true);
    entry->set_placeholder_text(_("placeholders.preferences.shortcut_entry"));
    entry->set_text(shortcutGet());
    entry->signal_icon_press().connect(sigc::bind(&onEntryIconActivated, entry));
    entry->signal_changed().connect(sigc::bind(&onEntryChanged, entry, shortcutSet));
}

void ShortcutGridRow::configureWidget(Gtk::Widget &widget)
{
    widget.set_size_request(-1, 32);
    widget.set_margin_start(5);
    widget.set_margin_bottom(5);

    widget.set_visible(true);
}

void ShortcutGridRow::attachTo(Glib::RefPtr<Gtk::Grid> &container, int index) const
{
    // width and height of controls added to the grid
    int ctrl_width = 1;
    int ctrl_height = 1;

    container->attach(*row_label, 0, index, ctrl_width, ctrl_height);
    container->attach(*row_main, 1, index, ctrl_width, ctrl_height);
    container->attach(*row_alt, 2, index, ctrl_width, ctrl_height);
}

ShortcutGridRow::~ShortcutGridRow()
{
}