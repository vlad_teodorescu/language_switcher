/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef SHORTCUTGRID_H
#define SHORTCUTGRID_H
#pragma once

#include "../../shortcuts/ShortcutRegistrar.h"
#include "ShortcutGridRow.h"
#include <gtkmm.h>
#include <memory>
#include <vector>

using namespace std;

class ShortcutGrid
{
public:
    /**
     * @brief Default constructor.
     */
    ShortcutGrid(Glib::RefPtr<Gtk::Builder> &builder, ShortcutRegistrar &registrar);

    void configureGrid();

    /**
     * @brief Default destructor.
     */
    ~ShortcutGrid();

protected:
    /**
     * @brief Calculates the grid's total number of rows, based on the assumption that the grid has NUM_COLUMNS columns.
     *
     * @return number of rows
     */
    int calcNumRows();

    /**
     * @brief Returns the GTK container in which the shortcut grid resides.
     *
     * @return container object
     */
    Glib::RefPtr<Gtk::Grid> getContainer();

    /**
     * @brief Removes all the shortcut entries in the grid. The grid header is left intact.
     */
    void clearEntries();

    /**
     * @brief Configures the grid header.
     */
    void configureHeader();

    /**
     * @brief Configures an individual cell of the grid header.
     *
     * @param widget_id widget ID of the label displayed in the given cell
     * @param text the text that should be displayed in the given cell
     * @param tooltip the tooltip that should be displayed (on mouseover) for the given cell
     */
    void configureHeaderCell(const string &widget_id, const string &text, const string &tooltip);

private:
    static inline const int NUM_COLUMNS = 3;

    Glib::RefPtr<Gtk::Builder> &builder;
    ShortcutRegistrar &registrar;
    vector<shared_ptr<ShortcutGridRow>> rows;
};

#endif // SHORTCUTGRID_H