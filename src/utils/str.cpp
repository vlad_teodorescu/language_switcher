/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "str.h"

#include <algorithm>
#include <sstream>
#include <iostream>

namespace utils
{
    namespace str
    {
        string join(vector<string> parts, string delimiter)
		{
			stringstream ss;
			for (int i = 0; i < parts.size(); i++)
			{
				if (i && delimiter.size())
					ss << delimiter;
				ss << parts[i];
			}

    		return ss.str();
		}

        vector<string> split(string s, string delimiter)
		{
			vector<string> result;
			size_t pos = 0;
			string token;
			
			if (delimiter.size())
			{
				while ((pos = s.find(delimiter)) != string::npos) 
				{
					token = s.substr(0, pos);
					result.push_back(token);
					s.erase(0, pos + delimiter.length());
				}
				if (s.length())
					result.push_back(s);
			}
			else
			{
				// empty delimiter would cause an infinite loop in the above logic block
				for (int i = 0; i < s.size(); i++)
					result.push_back(string(1, s[i]));
			}

			return result;
		}

    } // namespace string
} // namespace utils