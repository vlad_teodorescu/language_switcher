/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef UTILS_COMMON_H
#define UTILS_COMMON_H
#pragma once

#include <string>

using namespace std;

namespace utils
{
    namespace common
    {
        /**
         * @brief Checks if the given character is an ASCII leter (a-z, A-Z)
         * @param c character to check
         * @return true if ASCII letter, false otherwise
         */
        bool is_ascii_letter(char c);

        /**
         * @brief Checks if the given character is a digit (0-9)
         * @param c character to check
         * @return true if digit, false otherwise
         */
        bool is_digit(char c);

        /**
         * @brief Checks if the given string is a ISO 639-1 code (2-character ISO code)
         * @param dname string to check
         * @return true if valid ISO 639-1 code, false otherwise
         */
        bool is_iso639_code(const string &dname);
    } // namespace common
} // namespace utils

#endif // UTILS_COMMON_H
