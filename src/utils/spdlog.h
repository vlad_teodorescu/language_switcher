/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef UTILS_SPDLOG_H
#define UTILS_SPDLOG_H
#pragma once

#include <spdlog/common.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h> // or "../stdout_sinks.h" if no colors needed
#include <spdlog/sinks/syslog_sink.h>
#include <string>

using namespace std;

namespace utils
{
    namespace spdlog
    {
        /**
         * @brief Initializes a sink that writes to stdout.
         * @param sink_level lower threshold of message severity
         * @return initialized sink
         */
        shared_ptr<::spdlog::sinks::stdout_color_sink_mt> init_console_sink(const ::spdlog::level::level_enum sink_level);

        /**
         * @brief Initializes a sink that writes to the given file.
         * @param filename name of the file to write to
         * @param sink_level lower threshold of message severity
         * @return initialized sink
         */
        shared_ptr<::spdlog::sinks::basic_file_sink_mt> init_file_sink(const string &filename, const ::spdlog::level::level_enum sink_level);

        /**
         * @brief Initializes a sink that writes to syslog.
         * @param component name of the component, as it will be printed in syslog
         * @param sink_level lower threshold of message severity
         * @param options syslog options
         * @param facilty subsystem to which the messages belong
         * @return initialized sink
         */
        shared_ptr<::spdlog::sinks::syslog_sink_mt> init_syslog_sink(const string &component, const ::spdlog::level::level_enum sink_level,
                                                                     int options = LOG_CONS | LOG_PID, int facility = LOG_USER);

        shared_ptr<::spdlog::logger> init_multisink_logger(::spdlog::sinks_init_list sink_list);
    } // namespace spdlog
} // namespace utils

#endif // UTILS_SPDLOG_H
