/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef UTILS_FILE_H
#define UTILS_FILE_H
#pragma once

#include <string>

using namespace std;

namespace utils
{
	namespace file
	{
		/**
		 * @brief Creates a single folder. If the parent folder doesn't exist, the function fails.
		 * @param path path to folder
		 * @return true if folder has been created, false otherwise
		 */
		bool make_dir_single(const string& path);

		/**
		 * @brief Creates a folder, and all of its not yet existing parent folders.
		 * @param path path to folder
		 * @return true if folder has been created, false otherwise
		 */
		bool make_dir_recursive(const string& path);

		/**
		 * @brief Purges the given path, i.e. deletes whatever might be there, be it a file, folder pipe etc.
		 * @param path path to purge
		 * @return true if the operation succeded, false otherwise
		 */
		bool purge_path(const string& path);
	} // namespace file
} // namespace utils

#endif // UTILS_FILE_H
