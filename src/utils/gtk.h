/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef UTILS_GTK_H
#define UTILS_GTK_H
#pragma once

#include <gtkmm/widget.h>
#include <gtkmm/entry.h>
#include <gtkmm/label.h>
#include <gtkmm/window.h>

using namespace std;

namespace utils
{
    namespace gtk
    {
        void copy_widget_data(const Gtk::Widget *source, Gtk::Widget *target);

        void copy_entry_data(const Gtk::Entry *source, Gtk::Entry *target);

        void copy_label_data(const Gtk::Label *source, Gtk::Label *target);
        
        void copy_window_data(Gtk::Window *source, Gtk::Window *target);
    }
}

#endif // UTILS_GTK_H