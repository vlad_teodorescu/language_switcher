/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "spdlog.h"

using namespace std;

string pattern_default = "%d.%m.%Y %T [%^%l%$] %v";
string pattern_syslog = "[%^%l%$] %v";

namespace utils
{
    namespace spdlog
    {
        shared_ptr<::spdlog::sinks::stdout_color_sink_mt> init_console_sink(const ::spdlog::level::level_enum sink_level)
        {
            auto console_sink = make_shared<::spdlog::sinks::stdout_color_sink_mt>();
            console_sink->set_level(sink_level);
            console_sink->set_pattern(pattern_default);

            return console_sink;
        }

        shared_ptr<::spdlog::sinks::basic_file_sink_mt> init_file_sink(const string &filename, ::spdlog::level::level_enum sink_level)
        {
            auto file_sink = make_shared<::spdlog::sinks::basic_file_sink_mt>(filename, true);
            file_sink->set_level(sink_level);
            file_sink->set_pattern(pattern_default);

            return file_sink;
        }

        shared_ptr<::spdlog::sinks::syslog_sink_mt> init_syslog_sink(const string &component, ::spdlog::level::level_enum sink_level, int options,
                                                                     int facility)
        {
            auto syslog_sink = make_shared<::spdlog::sinks::syslog_sink_mt>(component, options, facility, true);
            syslog_sink->set_level(sink_level);
            syslog_sink->set_pattern(pattern_syslog);

            return syslog_sink;
        }

        shared_ptr<::spdlog::logger> init_multisink_logger(::spdlog::sinks_init_list sink_list)
        {
            auto logger = std::make_shared<::spdlog::logger>("", sink_list.begin(), sink_list.end());

            return logger;
        }
    } // namespace spdlog
} // namespace utils
