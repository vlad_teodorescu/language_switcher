/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "path.h"

#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <wordexp.h>
#include <spdlog/spdlog.h>
#include <sys/stat.h> // stat
#include <errno.h>    // errno, ENOENT, EEXIST
#include "../exceptions/SystemException.h"
#include "str.h"

#if defined(_WIN32)
#include <direct.h>   // _mkdir
#endif

#if defined(_WIN32)
	#define ___stat 	_stat
	#define ___S_IFDIR 	_S_IFDIR
	#define ___S_IFREG 	_S_IFREG
	#define ___S_IRUSR 	_S_IRUSR
#else
	#define ___stat 	stat
	#define ___S_IFDIR 	S_IFDIR
	#define ___S_IFREG 	S_IFREG
	#define ___S_IRUSR 	S_IRUSR
#endif


using namespace std;

inline bool _is_dir(const __mode_t& st_mode)
{
	return (st_mode & ___S_IFDIR) != 0;
}
inline bool _is_file(const __mode_t& st_mode)
{
	return (st_mode & ___S_IFREG) != 0;
}
inline bool _user_can_read(const __mode_t& st_mode)
{
	#if defined(_WIN32)
		return true;
	#endif
	return (st_mode & ___S_IRUSR) != 0;
}

namespace utils
{
	namespace path
	{
		string merge(const vector<string> &path_components)
        {
			return utils::str::join(path_components, PATH_SEPARATOR);
        }

        string get_filename(const string &path)
        {
			// remove trailing (back)slash
			string edited_path;
            if (*(path.rbegin()) == PATH_SEPARATOR[0])
                edited_path = path.substr(0, path.size() - 1);
            else
                edited_path = path;


            if (edited_path.empty())
                return "";
			
			vector<string> path_components = utils::str::split(edited_path, PATH_SEPARATOR);
			return path_components[path_components.size() - 1];
        }

		bool is_path_free(const string& path)
		{
			struct ___stat info;
			return (___stat(path.c_str(), &info) != 0);
		}

		bool is_path_readable(const string& path)
		{
			struct ___stat info;
			if (___stat(path.c_str(), &info) != 0)
			{
				return false;
			}

			if (!_user_can_read(info.st_mode))
			{
				return false;
			}

			return true;
		}

		bool is_path_file(const string& path)
		{
			struct ___stat info;
			if (___stat(path.c_str(), &info) != 0)
			{
				return false;
			}

			return _is_file(info.st_mode);
		}

		bool is_path_folder(const string& path)
		{
			struct ___stat info;
			if (___stat(path.c_str(), &info) != 0)
			{
				return false;
			}
			
			return _is_dir(info.st_mode);
		}

		int get_last_delimiter_index(const string& path)
		{
			int pos = path.find_last_of('/');
			#if defined(_WIN32)
				if (pos == string::npos)
				{
					pos = path.find_last_of('\\');
				}
			#endif

			return pos;
		}

		string expand_path(const string& path)
		{
			wordexp_t exp_result;
			int result = wordexp(path.c_str(), &exp_result, 0);

			if (result != 0)
			{
				wordfree(&exp_result);
				throw SystemException("Failed to expand path " + path);
			}

			string ret(exp_result.we_wordv[0]);
			wordfree(&exp_result);

			return ret;
		}
    } // namespace path
} // namespace utils
