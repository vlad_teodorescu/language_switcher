/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "common.h"

#include <string>

using namespace std;

namespace utils
{
    namespace common
    {
        // returns true if character is an ascii letter
        bool is_ascii_letter(char c)
        {
            return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'));
        }

        bool is_digit(char c)
        {
            return (c >= '0' && c <= '9');
        }

        bool is_iso639_code(const string &dname)
        {
            if (dname.length() != 2)
                return false;

            return is_ascii_letter(dname.at(0)) && is_ascii_letter(dname.at(1));
        }
    } // namespace common
} // namespace utils
