/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef UTILS_PATH_H
#define UTILS_PATH_H
#pragma once

#include <string>
#include <vector>

using namespace std;

namespace utils
{
	namespace path
	{
#if defined(_WIN32)
        const string PATH_SEPARATOR = "\\";
#else
        const string PATH_SEPARATOR = "/";
#endif

        /**
         * @brief Merges a bunch of path components into a string containing the final path.
         * @param path_parts path components to merge
         * @return final path, or empty if no components provided
         */
        string merge(const vector<string> &path_components);

        /**
         * @brief Returns the name of the file or folder pointed to by a path.
         *
         * @param path path for which the file/folder is to be returned
         * @return file/folder name
         */
        string get_filename(const string &path);

		/**
		 * @brief Checks if the given path is free (no file/folder present at location).
		 * @param path file path to check
		 * @return true if path is free, false otherwise
		 */
		bool is_path_free(const string& path);

		/**
		 * @brief Checks if the given path is readable (exists and is readable).
		 * @param path path to check
		 * @return true if path meets all conditions, false otherwise
		 */
		bool is_path_readable(const string& path);

		/**
		 * @brief Checks if the given path is a file.
		 * @param path path to check
		 * @return true if path is file, false otherwise
		 */
		bool is_path_file(const string& path);

		/**
		 * @brief Checks if the given path is a folder.
		 * @param path path to check
		 * @return true if path is folder, false otherwise
		 */
		bool is_path_folder(const string& path);

		/**
		 * @brief Calculates the position (the index) of the last delimiter in the path.
		 * @param path path to calculate last index of delimiter
		 * @return index of last delimiter
		 */
		int get_last_delimiter_index(const string& path);

		/**
		 * @brief Expands a path, replacing shortcuts like tilde with their actual path.
		 * 
		 * @param path path to expand
		 * @return expanded path (replaced shortcuts with actual path)
		 */
		string expand_path(const string& path);
	} // namespace path
} // namespace utils

#endif //UTILS_PATH_H

