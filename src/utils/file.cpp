/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "file.h"

#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <spdlog/spdlog.h>
#include <sys/stat.h> // stat
#include <errno.h>    // errno, ENOENT, EEXIST
#if defined(_WIN32)
#include <direct.h>   // _mkdir
#endif
#include "path.h"

mode_t DEFAULT_CREAT_MODE = 0755;


using namespace std;

namespace utils
{
	namespace file
	{
		bool make_dir_single(const string& path)
		{
			#if defined(_WIN32)
				int ret = _mkdir(path.c_str());
			#else
				int ret = mkdir(path.c_str(), DEFAULT_CREAT_MODE);
			#endif

			return ret == 0;
		}

		bool make_dir_recursive(const string& path)
		{
			if (utils::path::is_path_folder(path))
			{
				return true;
			}

			if (make_dir_single(path))
				return true;

			switch (errno)
			{
				case ENOENT:
					{
						// parent didn't exist, try to create it
						int pos = utils::path::get_last_delimiter_index(path);
						if (pos == string::npos)
							return false;

						if (!make_dir_recursive(path.substr(0, pos)))
							return false;
					}
					// now, try to create again
					return make_dir_single(path);
				case EEXIST:
					// done!
					return utils::path::is_path_folder(path);

				default:
					return false;
			}

			return false;
		}

		bool purge_path(const string& path)
		{
			try
			{
				std::filesystem::remove_all(path);
				return true;
			}
			catch(const std::filesystem::filesystem_error& e)
			{
				spdlog::warn(e.what());
				return false;
			}
		}
	} // namespace file
} // namespace utils
