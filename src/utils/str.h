/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef UTILS_STR_H
#define UTILS_STR_H
#pragma once

#include <string>
#include <vector>

using namespace std;

namespace utils
{
    namespace str
    {
        string join(vector<string> parts, string delimiter = "");

        vector<string> split(string s, string delimiter = "");

    } // namespace str
} // namespace utils

#endif // UTILS_STR_H