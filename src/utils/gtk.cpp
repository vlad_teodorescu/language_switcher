/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "gtk.h"

namespace utils
{
	namespace gtk
	{
		void copy_widget_data(const Gtk::Widget *source, Gtk::Widget *target)
		{
			target->set_app_paintable(source->get_app_paintable());
			target->set_can_default(source->get_can_default());
			target->set_can_focus(source->get_can_focus());
			target->set_default_direction(source->get_default_direction());
			target->set_focus_on_click(source->get_focus_on_click());
			target->set_halign(source->get_halign());
			target->set_hexpand(source->get_hexpand());
			target->set_margin_start(source->get_margin_start());
			target->set_margin_end(source->get_margin_end());
			target->set_margin_top(source->get_margin_top());
			target->set_margin_bottom(source->get_margin_bottom());
			target->set_opacity(source->get_opacity());
			target->set_receives_default(source->get_receives_default());
			target->set_sensitive(source->get_sensitive());
			target->set_valign(source->get_valign());
			target->set_vexpand(source->get_vexpand());
			target->set_visible(source->get_visible());

			int width = -1, height = -1;
			source->get_size_request(width, height);
			target->set_size_request(width, height);
		}

		void copy_entry_data(const Gtk::Entry *source, Gtk::Entry *target)
		{
			copy_widget_data(source, target);

			Gtk::StockID icon1 = source->get_icon_stock(Gtk::ENTRY_ICON_PRIMARY);
			
			if (icon1)
			{
				target->set_icon_from_stock(icon1, Gtk::ENTRY_ICON_PRIMARY);
				target->set_icon_activatable(source->get_icon_activatable(Gtk::ENTRY_ICON_PRIMARY), Gtk::ENTRY_ICON_PRIMARY);

				// there is a missing const qualifier in get_icon_sensitive	
				Gtk::Entry *source_n = const_cast<Gtk::Entry *>(source);
				target->set_icon_sensitive(Gtk::ENTRY_ICON_PRIMARY, source_n->get_icon_sensitive(Gtk::ENTRY_ICON_PRIMARY));
			}
			
			Gtk::StockID icon2 = source->get_icon_stock(Gtk::ENTRY_ICON_SECONDARY);
			if (icon2)
			{
				target->set_icon_from_stock(source->get_icon_stock(Gtk::ENTRY_ICON_SECONDARY), Gtk::ENTRY_ICON_SECONDARY);
				target->set_icon_activatable(source->get_icon_activatable(Gtk::ENTRY_ICON_SECONDARY), Gtk::ENTRY_ICON_SECONDARY);

				// there is a missing const qualifier in get_icon_sensitive	
				Gtk::Entry *source_n = const_cast<Gtk::Entry *>(source);
				target->set_icon_sensitive(Gtk::ENTRY_ICON_SECONDARY, source_n->get_icon_sensitive(Gtk::ENTRY_ICON_SECONDARY));
			}

			target->set_placeholder_text(source->get_placeholder_text());
			target->set_icon_activatable(source->get_icon_activatable());
		}

		void copy_label_data(const Gtk::Label *source, Gtk::Label *target)
		{
			copy_widget_data(source, target);

			target->set_justify(source->get_justify());
			target->set_text(source->get_text());
			target->set_tooltip_text(source->get_tooltip_text());
		}

		void copy_window_data(Gtk::Window *source, Gtk::Window *target)
		{
			copy_widget_data(source, target);

			target->set_resizable(source->get_resizable());
			target->set_deletable(source->get_deletable());
			target->set_icon_name(source->get_icon_name());
			target->set_icon(source->get_icon());
		}
	}
}