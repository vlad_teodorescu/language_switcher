/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef LOCALIZEDRESOURCESET_H
#define LOCALIZEDRESOURCESET_H
#pragma once

#include <dirent.h>
#include <string>
#include <vector>

#include "LocalizedResource.h"

using namespace std;
using namespace libconfig;

class LocalizedResourceSet
{

protected:
    // Stores layout ISO codes in the order that they were read from disk (since map is unordered)
    vector<LocalizedResource> layouts;

    // index of current layout (its position in the layouts vector)
    unsigned int curr_layout;

    string data_dir_path;

    /**
     * @brief Searches for available layouts and returns a vector of layout codes
     * @throw IoException if the folder(s) could not be opened for reading
     */
    vector<LocalizedResource> findAvailableLayouts() const;

    /**
     * @brief clears all data, and resets the current layout counter to 0
     */
    void clearData();

public:
    // Alias the const_iterator type to abstract its inner working
    typedef vector<LocalizedResource>::const_iterator const_iterator;

    // Alias the iterator type to abstract its inner working
    typedef vector<LocalizedResource>::iterator iterator;

    /**
     * @brief Default constructor. Initializes class
     */
    LocalizedResourceSet(string data_dir);

    /**
     * @brief Copy constructor.
     */
    LocalizedResourceSet(const LocalizedResourceSet &that);

    /**
     * @brief copy assignment operator
     */
    LocalizedResourceSet &operator=(const LocalizedResourceSet &that);

    /**
     * @brief Reloads all message sources from disk
     */
    void reload();

    /**
     * @brief Selects a certain layout based on its index.
     * @param index index to select
     * @throw NoSuchElementException if no layout found for respective index
     * @return true if the layout has been changed, false otherwise (e.g. lang was already selected)
     */
    bool select(const unsigned int index);

    /**
     * @brief Checks if a given index is the selected index.
     * @param index index to check
     * @return true if the layout is selected, false otherwise
     */
    bool isSelected(const unsigned int index);

    /**
     * @brief Selects a certain layout based on its layout code.
     * @param lang 2-letter iso code of layout to select
     * @throw NoSuchElementException if no layout found for respective code
     * @return true if the layout has been changed, false otherwise (e.g. lang was already selected)
     */
    bool select(const string &lang);

    /**
     * @brief Selects next layout. If the end of the array has been reached, loop to first layout
     */
    void next();

    /**
     * @brief Returns the currently selected layout.
     *
     * @return currently selected layout
     */
    LocalizedResource getCurrentLayout() const;

    const_iterator begin() const;
    const_iterator end() const;

    /**
     * @brief Destructor
     */
    ~LocalizedResourceSet();
};

#endif // LOCALIZEDRESOURCESET_H
