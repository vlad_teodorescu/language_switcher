/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "LocalizedResource.h"

#include <algorithm>
#include <dirent.h>
#include <iostream>
#include <libconfig.h++>
#include <map>
#include <spdlog/spdlog.h>
#include <string>
#include <vector>

#include "../exceptions/IoException.h"
#include "../exceptions/NoSuchElementException.h"
#include "../exceptions/SystemException.h"
#include "../utils/common.h"
#include "../utils/path.h"

using namespace std;

LocalizedResource::LocalizedResource(const string &data_dir, const string &code) : data_dir_path(data_dir), code(code)
{
    this->reload();
}

LocalizedResource::LocalizedResource(const LocalizedResource &that) : data_dir_path(that.data_dir_path), code(that.code)
{
    this->reload();
}

LocalizedResource &LocalizedResource::operator=(const LocalizedResource &that)
{
    if (this != &that)
    {
        this->data_dir_path = that.data_dir_path;
        this->code = that.code;

        this->reload();
    }

    return *this;
}

void LocalizedResource::reload()
{
    config.clear();
    initConfig(this->getConfigFilePath());
}

void LocalizedResource::initConfig(const string &file_name)
{
    try
    {
        config.readFile(file_name);
    }
    catch (const libconfig::FileIOException &e)
    {
        throw IoException("I/O error while reading " + file_name + ": " + e.what());
    }
    catch (const libconfig::ParseException &e)
    {
        throw SystemException("Parse error at [" + file_name + ":" + to_string(e.getLine()) + "]: " + e.getError());
    }
}

string LocalizedResource::getCfg(const string &key) const
{
    try
    {
        return config.lookup(key);
    }
    catch (const libconfig::SettingNotFoundException &e)
    {
        throw NoSuchElementException("Config key " + key + " does not exist for layout " + this->code);
    }
}

string LocalizedResource::getIconPath() const
{
    return utils::path::merge({this->data_dir_path, FNAME_ICON});
}

string LocalizedResource::getScriptFilePath() const
{
    return utils::path::merge({this->data_dir_path, FNAME_SCRIPT});
}

string LocalizedResource::getConfigFilePath() const
{
    return utils::path::merge({this->data_dir_path, FNAME_LOCALES});
}

string LocalizedResource::getCode() const
{
    return this->code;
}

string LocalizedResource::getName() const
{
    return this->getCfg(CFG_LAYOUT_NAME);
}

LocalizedResource::~LocalizedResource()
{
}

bool LocalizedResource::canLoadAsResourceFolder(const string &path)
{
    string dirname = utils::path::get_filename(path);
    DIR *subdir = opendir(path.c_str());

    if (subdir == NULL)
    {
        spdlog::warn("Ignoring entry {}, not found or not a folder", dirname);
        return false;
    }

    if (!utils::common::is_iso639_code(dirname))
    {
        spdlog::info("Ignoring entry {}, folder name not an ISO code", dirname);
        return false;
    }

    if (!hasRequiredResources(subdir))
    {
        spdlog::info("Igonring entry {}, resources not found", dirname);
        return false;
    }

    closedir(subdir);

    return true;
}

bool LocalizedResource::hasRequiredResources(DIR *opened_subdir)
{
    struct dirent *subentry;
    int nMatches = 0;
    while ((subentry = readdir(opened_subdir)) != NULL)
    {
        if (string(subentry->d_name).compare(FNAME_SCRIPT) == 0)
            nMatches++;
        else if (string(subentry->d_name).compare(FNAME_LOCALES) == 0)
            nMatches++;
        else if (string(subentry->d_name).compare(FNAME_ICON) == 0)
            nMatches++;
    }

    return nMatches == 3;
}
