/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef LOCALIZEDRESOURCE_H
#define LOCALIZEDRESOURCE_H
#pragma once

#include <dirent.h>
#include <libconfig.h++>
#include <string>

using namespace std;

class LocalizedResource
{

protected:
    static inline const string FNAME_ICON = "flag.png";
    static inline const string FNAME_SCRIPT = "xmodmaprc";
    static inline const string FNAME_LOCALES = "layout.conf";
    static inline const string CFG_LAYOUT_NAME = "layout.name";

    libconfig::Config config;

    string data_dir_path;
    string code;

    /**
     * @brief Initializes the config object from the given file.
     *
     * @param file_name file from which the configuration is read
     */
    void initConfig(const string &file_name);

    /**
     * @brief Checks if the given folder has the required resources to be loaded as a layout folder.
     * @param subdir entry to check
     * @return true if all resources are present, false otherwise
     */
    static bool hasRequiredResources(DIR *subdir);

public:
    /**
     * @brief Default constructor. Initializes class
     */
    LocalizedResource(const string &data_dir, const string &code);

    /**
     * @brief Copy constructor.
     */
    LocalizedResource(const LocalizedResource &that);

    /**
     * @brief copy assignment operator
     */
    LocalizedResource &operator=(const LocalizedResource &that);

    /**
     * @brief Reloads all message sources from disk
     */
    void reload();

    /**
     * @brief For a given key, returns the configuration value
     * @param key key for which the configuration is to be retrieved
     * @return configuration value
     * @throw NoSuchElementException if no configuration could be found for given key
     */
    string getCfg(const string &key) const;

    /**
     * @brief Returns the full path to this layout's icon file
     * @return absolute path to file
     */
    string getIconPath() const;

    /**
     * @brief Returns the full path to this layout's Xmodmap script file
     * @return absolute path to file
     */
    string getScriptFilePath() const;

    /**
     * @brief Returns the full path to this layout's translations file
     * @return absolute path to file
     */
    string getConfigFilePath() const;

    /**
     * @brief Returns 2-letter code of current layout
     */
    string getCode() const;

    /**
     * @brief Returns the name of the current layout
     * @return string layout name
     */

    string getName() const;

    /**
     * @brief Destructor
     */
    ~LocalizedResource();

    /**
     * @brief Checks if the given folder fits the requirements to be loaded as a layout folder
     * @param path absolute path to the folder which should be checked
     * @return true if folder can be used as layout folder, false otherwise
     */
    static bool canLoadAsResourceFolder(const string &path);
};

#endif // LOCALIZEDRESOURCE_H