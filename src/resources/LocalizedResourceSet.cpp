/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "LocalizedResourceSet.h"

#include <algorithm>
#include <dirent.h>
#include <iostream>
#include <libconfig.h++>
#include <map>
#include <spdlog/spdlog.h>
#include <string>
#include <vector>

#include "../exceptions/IoException.h"
#include "../exceptions/NoSuchElementException.h"
#include "../exceptions/SystemException.h"
#include "../utils/common.h"
#include "../utils/path.h"

using namespace std;
using namespace libconfig;

LocalizedResourceSet::LocalizedResourceSet(string data_dir) : data_dir_path{data_dir}
{
    this->reload();
}

LocalizedResourceSet::LocalizedResourceSet(const LocalizedResourceSet &that) : data_dir_path{that.data_dir_path}
{
    this->reload();
}

LocalizedResourceSet &LocalizedResourceSet::operator=(const LocalizedResourceSet &that)
{
    if (this != &that)
    {
        this->data_dir_path = that.data_dir_path;
        this->reload();
    }

    return *this;
}

vector<LocalizedResource> LocalizedResourceSet::findAvailableLayouts() const
{
    DIR *dir;
    struct dirent *subdir;
    vector<LocalizedResource> resources;

    if ((dir = opendir(data_dir_path.c_str())) == NULL)
        throw IoException("Could not open layout dir for reading: " + data_dir_path);

    /* print all the files and directories within directory */
    while ((subdir = readdir(dir)) != NULL)
    {
        string subdir_abs_path = utils::path::merge({data_dir_path, string(subdir->d_name)});
        if (!LocalizedResource::canLoadAsResourceFolder(subdir_abs_path))
        {
            spdlog::debug("Subfolder {} is not a layout folder, ignoring", subdir->d_name);
            continue;
        }

        // we convert the name to lowercase since we do not want duplicates, and the easiest way to
        // accomplish that is to only store lowercase codes
        string langdir_name(subdir->d_name);
        transform(langdir_name.begin(), langdir_name.end(), langdir_name.begin(), ::tolower);

        for (vector<LocalizedResource>::iterator it = resources.begin(); it != resources.end(); it++)
        {
            // search the ret array for possible duplicates
            // *nix machines are case sensitive, thus it is possible to have 2 folders de and DE
            if (langdir_name.compare(it->getCode()) == 0)
            {
                spdlog::warn("Igonring entry {}, already exists", subdir->d_name);
                continue;
            }
        }

        spdlog::info("Found layout folder {}, adding to collection", langdir_name);

        LocalizedResource aux(subdir_abs_path, langdir_name);
        resources.push_back(aux);
    }

    closedir(dir);

    return resources;
}

LocalizedResource LocalizedResourceSet::getCurrentLayout() const
{
    return this->layouts.at(this->curr_layout);
}

void LocalizedResourceSet::reload()
{
    this->clearData();

    this->layouts = findAvailableLayouts();

    if (!this->layouts.size())
        throw SystemException("Data folder does not contain any layouts: " + data_dir_path);
}

void LocalizedResourceSet::clearData()
{
    this->layouts.clear();
    this->curr_layout = 0;
}

bool LocalizedResourceSet::select(const string &layout)
{
    unsigned int foundindex = 0;
    vector<LocalizedResource>::const_iterator it;

    for (it = this->layouts.begin(); it != this->layouts.end(); ++it)
    {
        if (it->getCode().compare(layout) == 0)
            return this->select(foundindex);

        foundindex++;
    }

    throw NoSuchElementException("Could not select layout because it was not found: " + layout);
}
bool LocalizedResourceSet::select(const unsigned int index)
{
    spdlog::info("Selecting layout {}", index);
    if (index < 0 || index >= this->layouts.size())
        throw NoSuchElementException("Invalid index " + to_string(index) + ", either negative or larger than number of layouts");

    bool ret = this->curr_layout != index;
    this->curr_layout = index;
    return ret;
}

bool LocalizedResourceSet::isSelected(const unsigned int index)
{
    return this->curr_layout == index;
}

void LocalizedResourceSet::next()
{
    spdlog::info("Selecting next layout");
    if (++this->curr_layout >= this->layouts.size())
        this->curr_layout = 0;
}

LocalizedResourceSet::const_iterator LocalizedResourceSet::begin() const
{
    return this->layouts.begin();
}

LocalizedResourceSet::const_iterator LocalizedResourceSet::end() const
{
    return this->layouts.end();
}

LocalizedResourceSet::~LocalizedResourceSet()
{
    this->clearData();
}
