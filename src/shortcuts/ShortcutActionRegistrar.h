/**
 *   Copyright (C) 2020  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef SHORTCUTACTIONREGISTRAR_H
#define SHORTCUTACTIONREGISTRAR_H
#pragma once

#include "ShortcutId.h"
#include "Shortcut.h"
#include <functional>
#include <map>
#include <string>

using namespace std;

class ShortcutActionRegistrar
{
protected:
    typedef map<reference_wrapper<const ShortcutId>, ShortcutAction> data_store;
    data_store bindings;

public:
    typedef data_store::const_iterator const_iterator;
    typedef data_store::iterator iterator;

    /**
     * @brief Associates a ShortcutAction with a given shortcut ID.
     *
     * @param shortcut_id shortcut ID to which the action is assigned
     * @param action shortcut action to perform when one of the combinations for the shortcut with the given ID is activated
     */
    void add(const ShortcutId &shortcut_id, const ShortcutAction action);

    /**
     * @brief Removes the action associated to a shortcut ID.
     *
     * @param shortcut_id shortcut ID
     */
    void remove(const ShortcutId &shortcut_id);

    /**
     * @brief Clears all associations between shortcut IDs and actions.
     */
    void clear();

    /**
     * @brief Returns all the associations known to the registrar in the form of a shortcut-action map.
     * 
     * @return shortcut-action associations map
     */
    ShortcutActionMap getAsMap() const;

    /**
     * @brief Searches for the action associated with the given shortcut canonical code.
     *
     * @param canonical_code shortcut's canonical code
     * @return ShortcutAction& reference to the ShortcutAction, if found
     * @throws NoSuchElementException, if no association found for the given canonical code
     */
    const ShortcutAction &find(const string &canonical_code) const;

    /**
     * @brief Default destructor.
     */
    virtual ~ShortcutActionRegistrar(){};

    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
};

#endif // SHORTCUTACTIONREGISTRAR_H
