/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "ShortcutGroup.h"
#include <spdlog/spdlog.h>
#include "../utils/str.h"
#include "../exceptions/NoSuchElementException.h"
#include "../exceptions/SystemException.h"

using namespace std;

ShortcutGroup::ShortcutGroup(const libconfig::Setting &node, const ShortcutGroupEntry *parent)
    : node(&node), ShortcutGroupEntry(node.getName(), parent)
{
    if (!node.isGroup())
        throw SystemException("Group " + code + ": Expected node to be a group");

    if (Shortcut::isShortcutConfigNode(node))
        throw SystemException("Group " + code + ": Expected node to be a shortcut group configuration, instead is a shortcut configuration node");

    this->load();
}

ShortcutGroup::ShortcutGroup(const ShortcutGroup &that, const ShortcutGroupEntry *parent): ShortcutGroupEntry(that.code, parent)
{
    for (auto it = that.subgroups.begin(); it != that.subgroups.end(); it++)
        subgroups.push_back(make_unique<ShortcutGroup>(**it, this));

    for (auto it = that.shortcuts.begin(); it != that.shortcuts.end(); it++)
        shortcuts.push_back(make_unique<Shortcut>(**it, this));
}

Shortcut &ShortcutGroup::lookup(const vector<string> &path) const
{
    if (path.empty())
        throw NoSuchElementException("Group " + code + ": cannot lookup shortcut for empty path");

    const string first_seg = path[0];

    if (path.size() == 1)
        return this->lookupInShortcuts(first_seg);

    vector<string>::const_iterator new_begin = path.begin() + 1;
    vector<string>::const_iterator new_end = path.end();
    vector<string> new_path(new_begin, new_end);

    return this->lookupInSubgroups(first_seg).lookup(new_path);
}

Shortcut &ShortcutGroup::lookupInShortcuts(const string code) const
{
    for (vector<unique_ptr<Shortcut>>::const_iterator it = shortcuts.begin(); it != shortcuts.end(); it++)
    {
        if (code == (*it)->getCode())
            return **it;
    }

    spdlog::info("Size is {}", this->shortcuts.size());

    throw NoSuchElementException("Group " + code + ": no shortcut found with the code '" + code + "'");
}

ShortcutGroup &ShortcutGroup::lookupInSubgroups(const string code) const
{
    for (vector<unique_ptr<ShortcutGroup>>::const_iterator it = subgroups.begin(); it != subgroups.end(); it++)
    {
        if (code == (*it)->getCode())
            return **it;
    }

    throw NoSuchElementException("Group " + code + ": no subgroup found with the code '" + code + "'");
}

Shortcut &ShortcutGroup::lookup(const ShortcutId &id) const
{
    return this->lookup(utils::str::split(id.getCode(), "."));
}

void ShortcutGroup::load()
{
    for (libconfig::Setting::const_iterator it = node->begin(); it != node->end(); it++)
    {
        if (Shortcut::isShortcutConfigNode(*it))
        {
            shortcuts.push_back(make_unique<Shortcut>(*it, this));
        }
        else if (it->isGroup())
        {
            subgroups.push_back(make_unique<ShortcutGroup>(*it, this));
        }
    }
}

void ShortcutGroup::save()
{
    for (auto it = this->subgroups.begin(); it != this->subgroups.end(); it++)
        (*it)->save();

    for (auto it = this->shortcuts.begin(); it != this->shortcuts.end(); it++)
        (*it)->save();
}

void ShortcutGroup::mergeFrom(const ShortcutGroup &that)
{
    for (auto it = this->subgroups.begin(); it != this->subgroups.end(); it++)
        (*it)->mergeFrom(that.lookupInSubgroups((*it)->getCode()));

    for (auto it = this->shortcuts.begin(); it != this->shortcuts.end(); it++)
        (*it)->mergeFrom(that.lookupInShortcuts((*it)->getCode()));
}

void ShortcutGroup::bindAll(const ShortcutActionMap &actions)
{
    for (auto it = this->subgroups.begin(); it != this->subgroups.end(); it++)
        (*it)->bindAll(actions);

    for (auto it = this->shortcuts.begin(); it != this->shortcuts.end(); it++)
        (*it)->bind(actions);
}

void ShortcutGroup::unbindAll(const ShortcutActionMap &actions)
{
    for (auto it = this->subgroups.begin(); it != this->subgroups.end(); it++)
        (*it)->unbindAll(actions);

    for (auto it = this->shortcuts.begin(); it != this->shortcuts.end(); it++)
        (*it)->unbind(actions);
}
