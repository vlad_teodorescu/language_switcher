/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "ShortcutRegistrar.h"

#include <spdlog/spdlog.h>

#include "../exceptions/NoSuchElementException.h"
#include "../exceptions/SystemException.h"

unique_ptr<ShortcutRegistrar> ShortcutRegistrar::instance;

ShortcutRegistrar::ShortcutRegistrar(ShortcutGroup & shortcuts): shortcuts(shortcuts)
{
}

void ShortcutRegistrar::registerCallback(const ShortcutId &id, ShortcutAction callback)
{
    this->actions.add(id, callback);
}

void ShortcutRegistrar::bindAll() const
{
    this->shortcuts.bindAll(this->actions.getAsMap());
}

void ShortcutRegistrar::unbindAll() const
{
    this->shortcuts.unbindAll(this->actions.getAsMap());
}

void ShortcutRegistrar::initDraftShortcuts()
{
    this->draft_shortcuts = make_unique<ShortcutGroup>(this->shortcuts, nullptr);
}

ShortcutGroup &ShortcutRegistrar::getDraftShortcuts()
{
    if (!this->draft_shortcuts)
        this->initDraftShortcuts();

    return *this->draft_shortcuts;
}

void ShortcutRegistrar::mergeDraftShortcuts()
{
    if (!this->draft_shortcuts)
        throw NoSuchElementException("No shortcut drafting in progress, cannot merge draft shorcuts");

    this->unbindAll();
    this->shortcuts.mergeFrom(*this->draft_shortcuts);
    this->bindAll();
    this->shortcuts.save();

    this->discardDraftShortcuts();
}

void ShortcutRegistrar::discardDraftShortcuts()
{
    this->draft_shortcuts.reset();
}

void ShortcutRegistrar::initialize(ShortcutGroup & shortcuts)
{
    if (ShortcutRegistrar::instance)
        throw SystemException("Shortcut registrar already initialized, cannot reinitialize");

    ShortcutRegistrar::instance = make_unique<ShortcutRegistrar>(shortcuts);
}

ShortcutRegistrar &ShortcutRegistrar::get()
{
    if (!ShortcutRegistrar::instance)
        throw SystemException("Shortcut registrar not initialized");

    return *(ShortcutRegistrar::instance);
}

ShortcutRegistrar::actions_iterator ShortcutRegistrar::beginActions()
{
    return this->actions.begin();
}

ShortcutRegistrar::actions_iterator ShortcutRegistrar::endActions()
{
    return this->actions.end();
}

ShortcutRegistrar::const_actions_iterator ShortcutRegistrar::beginActions() const
{
    return this->actions.begin();
}

ShortcutRegistrar::const_actions_iterator ShortcutRegistrar::endActions() const
{
    return this->actions.end();
}