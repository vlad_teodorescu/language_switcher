/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "Shortcut.h"
#include "../exceptions/NoSuchElementException.h"
#include "../exceptions/SystemException.h"
#include <keybinder.h>
#include <spdlog/spdlog.h>

using namespace std;

namespace
{
    bool keybinder_initialized = false;
    bool keybinder_available = false;

    void handleKeypress(const char *key_combo, void *data)
    {
        const ShortcutAction *func = static_cast<const ShortcutAction *>(data);

        (*func)();
    }

    bool isKeybinderAvailable()
    {
        if (!keybinder_initialized)
        {
            keybinder_initialized = true;

            if (keybinder_supported())
            {
                keybinder_init();
                keybinder_set_use_cooked_accelerators(false);
                keybinder_available = true;
            }
        }

        return keybinder_available;
    }

    void doBind(const string &shortcut, const ShortcutAction *action)
    {
        if (shortcut.empty())
            return;

        if (!isKeybinderAvailable())
        {
            spdlog::warn("libkeybinder not available, will skip binding of {}", shortcut);
            return;
        }

        keybinder_bind(shortcut.c_str(), handleKeypress, (void *)action);
    }

    void doUnbind(const string &shortcut)
    {
        if (shortcut.empty())
            return;

        if (!isKeybinderAvailable())
        {
            spdlog::warn("libkeybinder not available, will skip unbinding of {}", shortcut);
            return;
        }

        keybinder_unbind_all(shortcut.c_str());
    }

} // namespace

Shortcut::Shortcut(const libconfig::Setting &node, const ShortcutGroupEntry *parent)
    : node(&node), action(nullptr), ShortcutGroupEntry(node.getName(), parent)
{
    this->load();
}

Shortcut::Shortcut(const Shortcut &that, const ShortcutGroupEntry *parent) : node(nullptr), action(nullptr), ShortcutGroupEntry(that.code, parent)
{
    this->value_main = that.value_main;
    this->value_alt = that.value_alt;
}

bool Shortcut::isShortcutConfigNode(const libconfig::Setting &node)
{
    if (!node.isGroup() || node.getLength() != 2)
        return false;

    if (!node.exists(NODE_MAIN) || node.lookup(NODE_MAIN).getType() != libconfig::Setting::Type::TypeString)
        return false;

    if (!node.exists(NODE_ALT) || node.lookup(NODE_ALT).getType() != libconfig::Setting::Type::TypeString)
        return false;

    return true;
}

string Shortcut::getMain() const
{
    return this->value_main;
}

void Shortcut::setMain(string value)
{
    this->value_main = value;
}

string Shortcut::getAlt() const
{
    return this->value_alt;
}

void Shortcut::setAlt(string value)
{
    this->value_alt = value;
}

void Shortcut::load()
{
    if (!node)
        throw SystemException("Cannot load shortcut " + this->getCanonicalCode() + ", no configuration node attached to it");

    this->value_main = node->lookup(NODE_MAIN).c_str();
    this->value_alt = node->lookup(NODE_ALT).c_str();
}

void Shortcut::save()
{
    if (!node)
        throw SystemException("Cannot save shortcut " + this->getCanonicalCode() + ", no configuration node attached to it");

    node->lookup(NODE_MAIN) = this->value_main;
    node->lookup(NODE_ALT) = this->value_alt;
}

void Shortcut::mergeFrom(const Shortcut &that)
{
    this->value_main = that.value_main;
    this->value_alt = that.value_alt;
}

void Shortcut::bind(const ShortcutActionMap &actions)
{
    map<string, ShortcutAction>::const_iterator entry = actions.find(this->getCanonicalCode());
    if (entry == actions.end())
        throw NoSuchElementException("Could not find shortcut with code " + this->getCanonicalCode() + " in action map");

    this->action = new ShortcutAction(entry->second);
    doBind(this->value_main, this->action);
    doBind(this->value_alt, this->action);
}

void Shortcut::unbind(const ShortcutActionMap &actions)
{
    doUnbind(this->value_main);
    doUnbind(this->value_alt);
    this->action = nullptr;
}

bool operator<(const Shortcut &s1, const Shortcut &s2)
{
    return s1.code < s2.code;
}

bool operator>(const Shortcut &s1, const Shortcut &s2)
{
    return s1.code > s2.code;
}