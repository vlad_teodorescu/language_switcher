/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "ShortcutId.h"
#include "../i18n/i18n.h"


ShortcutId::ShortcutId(const string code, const string name, const string description): code(code), name(name), description(description)
{
	// nothing
}

string ShortcutId::getCode() const
{
	return this->code;
}

string ShortcutId::getName() const
{
	return _(this->name.c_str());
}

string ShortcutId::getDescription() const
{
	return _(this->description.c_str());
}

ShortcutId ShortcutId::make(const string code, const string name)
{
	return make(code, name, "");
}

ShortcutId ShortcutId::make(const string code, const string name, const string description)
{
	return ShortcutId(code, name, description);
}

bool operator<(const ShortcutId &s1, const ShortcutId &s2)
{
    return s1.code < s2.code;
}

bool operator>(const ShortcutId &s1, const ShortcutId &s2)
{
    return s1.code > s2.code;
}