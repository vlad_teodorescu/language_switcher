/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef SHORTCUTKEY_H
#define SHORTCUTKEY_H
#pragma once

#include <string>

using namespace std;

class ShortcutId {

private:
	const string code;
	const string name;
	const string description;

	/**
	 * @brief Construct a new ShortcutId object.
	 * 
	 * @param code canonical code of shortcut (including parent groups)
	 * @param name shortcut name
	 * @param description shortcut description
	 */
	ShortcutId(const string code, const string name, const string description);

public:
	/**
	 * @brief Returns the canonical code of the ShortcutId object.
	 * 
	 * @return canonical code of ShortcutId object
	 */
	string getCode() const;

	/**
	 * @brief Returns the name of the ShortcutId object.
	 * 
	 * @return shortcut name
	 */
	string getName() const;

	/**
	 * @brief Returns the description of the ShortcutId object.
	 * 
	 * @return shortcut description
	 */
	string getDescription() const;

	/**
	 * @brief Constructs a new ShortcutId object.
	 * 
	 * @param code canonical code of shortcut (including parent groups)
	 * @param name shortcut name 
	 * @return ShortcutId 
	 */
	static ShortcutId make(const string canonical_code, const string name);

	/**
	 * @brief Constructs a new ShortcutId object.
	 * 
	 * @param code canonical code of shortcut (including parent groups)
	 * @param name shortcut name 
	 * @param description shortcut description
	 * @return ShortcutId 
	 */
	static ShortcutId make(const string canonical_code, const string name, const string description);

	friend bool operator< (const ShortcutId &s1, const ShortcutId &s2);

    friend bool operator> (const ShortcutId &s1, const ShortcutId &s2);
};

#endif // SHORTCUTKEY_H
