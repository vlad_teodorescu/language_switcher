/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef SHORTCUTREGISTRAR_H
#define SHORTCUTREGISTRAR_H
#pragma once

#include "../shortcuts/ShortcutGroup.h"
#include "ShortcutActionRegistrar.h"
#include <memory>

using namespace std;

class ShortcutRegistrar
{

private:
    static unique_ptr<ShortcutRegistrar> instance;

    ShortcutGroup & shortcuts;
    ShortcutActionRegistrar actions;

    unique_ptr<ShortcutGroup> draft_shortcuts;

    ShortcutRegistrar(const ShortcutRegistrar &that);

    ShortcutRegistrar &operator=(const ShortcutRegistrar &that);

protected:
    /**
     * @brief Initializes a draft copy of the top-level shortcut group that can be used to make various changes to the shortcuts in it and then merge
     * them into the active shortcut group, or alternatively discard them.
     */
    void initDraftShortcuts();

public:
    typedef ShortcutActionRegistrar::const_iterator const_actions_iterator;
    typedef ShortcutActionRegistrar::iterator actions_iterator;

    /**
     * @brief Construct a new Shortcut Registrar object
     *
     * @param shortcuts top-level shortcut group
     */
    ShortcutRegistrar(ShortcutGroup & shortcuts);

    /**
     * @brief Registers a given callback for a shortcut ID. The registered callback will not go into effect immediately, but when it is bound to the
     * underlying keybinder library.
     *
     * @param id shortcut ID, based on which the actual shortcut is retrieved
     * @param callback callback to register
     * @throws NoSuchElemevoid ShortcutRegistrar::bindAll()
{
    this->shortcuts.bindAll(this->actions);
}ntException if no shortcut could be found with the given ID
     */
    void registerCallback(const ShortcutId &id, ShortcutAction callback);

    /**
     * @brief Binds all shortcut combinations to their respectively assigned actions.
     */
    void bindAll() const;

    /**
     * @brief Unbinds all shortcut combinations from their respectively assigned actions.
     */
    void unbindAll() const;

    /**
     * @brief Retrieves a draft copy of the current top-level shortcut group. If no draft exists, it is first initialized.
     *
     * @return ShortcutGroup& draft copy of top-level shortcut group
     */
    ShortcutGroup &getDraftShortcuts();

    /**
     * @brief Merges the draft copy of the top-level shortcut group into the working top-level shortcut group, recursively transfering the values of
     * the shortcuts in the draft to the working shortcuts. The draft is then discarded.
     */
    void mergeDraftShortcuts();

    /**
     * @brief Discards the current draft copy of the top-level shortcut group without merging any values into the working top-level shortcut group.
     */
    void discardDraftShortcuts();

    /**
     * @brief Initializes the singleton instance based on the given configuration node (from which the shortcut values are read).
     *
     * @param shortcuts top-level shortcuts group
     */
    static void initialize(ShortcutGroup & shortcuts);

    /**
     * @brief Returns a reference to the instantiated singleton.
     *
     * @return singleton instance
     * @throws SystemException if singleton not initialized
     */
    static ShortcutRegistrar &get();

    actions_iterator beginActions();
    actions_iterator endActions();
    const_actions_iterator beginActions() const;
    const_actions_iterator endActions() const;
};

#endif // SHORTCUTREGISTRAR_H
