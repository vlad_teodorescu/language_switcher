/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef SHORTCUTGROUP_H
#define SHORTCUTGROUP_H
#pragma once

#include <libconfig.h++>
#include <memory>
#include <vector>

#include "Shortcut.h"
#include "ShortcutGroupEntry.h"
#include "ShortcutId.h"

using namespace std;

class ShortcutGroup : public ShortcutGroupEntry
{
private:
    ShortcutGroup(const ShortcutGroup &that);

    ShortcutGroup &operator=(const ShortcutGroup &that);

    const libconfig::Setting * node;
    vector<unique_ptr<ShortcutGroup>> subgroups;
    vector<unique_ptr<Shortcut>> shortcuts;

protected:
    /**
     * @brief Looks up the given shortcut based on a list of simple codes, and returns a reference to the object (if found). Every element in the list
     * is assumed to be a subgroup except the last (which is assumed to be a shortcut).
     *
     * @param id shortcut to lookup
     * @return reference to shortcut object
     * @throws NoSuchElementException if no such shortcut found, or list empty
     */
    Shortcut &lookup(const vector<string> &path) const;

    /**
     * @brief Looks up the given shortcut based on the shortcut code, and returns a reference to the object (if found). The code of the shortcut must
     * be simple (paths are not resolved).
     *
     * @param code shortcut to lookup
     * @return reference to shortcut object
     * @throws NoSuchElementException if no such shortcut found
     */
    Shortcut &lookupInShortcuts(const string code) const;

    /**
     * @brief Looks up the given shortcut group based on the shortcut group code, and returns a reference to the object (if found). The code of the
     * shortcut group must be simple (paths are not resolved).
     *
     * @param code shortcut to lookup
     * @return reference to shortcut object
     * @throws NoSuchElementException if no such shortcut found
     */
    ShortcutGroup &lookupInSubgroups(const string code) const;

public:
    /**
     * @brief Constructs a new shortcut group object from scratch.
     *
     * @param node libconfig configuration node that holds shortcut group configuration
     * @param parent parent to which this shortcut group belongs
     * @param actions action map which is used to bind the group's shortcuts to a specific action
     */
    ShortcutGroup(const libconfig::Setting &node, const ShortcutGroupEntry *parent);

    /**
     * @brief Constructs a new shortcut group based on an existing shortcut group object.
     *
     * @param that shortcut group object that serves as a blueprint
     * @param parent parent to which this shortcut group belongs
     */
    ShortcutGroup(const ShortcutGroup &that, const ShortcutGroupEntry *parent);

    /**
     * @brief Looks up the given shortcut based on the shortcut id, and returns a reference to the object (if found). The shortcut id must contain the
     * fully qualified name of the shortcut (including parent groups) relative to this subgroup. If the code contains paths, these are resolved
     * accordingly.
     *
     * @param id shortcut to lookup
     * @return reference to shortcut object
     * @throws NoSuchElementException if no such shortcut found
     */
    Shortcut &lookup(const ShortcutId &id) const;

    /**
     * @brief Loads shortcuts and their values from the configuration.
     */
    void load();

    /**
     * @brief Writes shortcut values to the configuration.
     */
    void save();

    /**
     * @brief Merges the shortcut values of the other shortcut group into its own shortcut values.
     *
     * @param that other shortcut group from which the shortcut values should be merged
     */
    void mergeFrom(const ShortcutGroup &that);

    /**
     * @brief Binds the group's (and all subgroup's) shortcut combinations to their respectively assigned actions.
     * 
     * @param actions action map containing the mapping between shortcut IDs and the action that they should perform
     */
    void bindAll(const ShortcutActionMap &actions);

    /**
     * @brief Unbinds the group's (and all subgroup's) shortcut combinations from their respectively assigned actions.
     * 
     * @param actions action map containing the mapping between shortcut IDs and the action that they should perform
     */
    void unbindAll(const ShortcutActionMap &actions);
};

#endif // SHORTCUTGROUP_H
