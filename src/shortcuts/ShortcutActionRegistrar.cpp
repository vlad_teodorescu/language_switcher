/**
 *   Copyright (C) 2022  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "ShortcutActionRegistrar.h"
#include "../exceptions/NoSuchElementException.h"

void ShortcutActionRegistrar::add(const ShortcutId &shortcut_id, const ShortcutAction action)
{
    this->bindings[shortcut_id] = action;
}

void ShortcutActionRegistrar::remove(const ShortcutId &shortcut_id)
{
    if (this->bindings.find(shortcut_id) == this->bindings.end())
        return;

    this->bindings.erase(shortcut_id);
}

void ShortcutActionRegistrar::clear()
{
    for (auto it = this->bindings.begin(); it != this->bindings.end(); it++)
        this->remove(it->first);
}

ShortcutActionMap ShortcutActionRegistrar::getAsMap() const
{
    ShortcutActionMap map;
    for (auto it = this->bindings.begin(); it != this->bindings.end(); it++)
        map[it->first.get().getCode()] = it->second;
    
    return map;
}

const ShortcutAction &ShortcutActionRegistrar::find(const string &id) const
{
    for (auto it = this->bindings.begin(); it != this->bindings.end(); it++)
        if (it->first.get().getCode() == id)
            return it->second;

    throw NoSuchElementException("No shortcut action registered for shortcut id " + id);
}

ShortcutActionRegistrar::iterator ShortcutActionRegistrar::begin()
{
    return this->bindings.begin();
}

ShortcutActionRegistrar::iterator ShortcutActionRegistrar::end()
{
    return this->bindings.end();
}

ShortcutActionRegistrar::const_iterator ShortcutActionRegistrar::begin() const
{
    return this->bindings.begin();
}

ShortcutActionRegistrar::const_iterator ShortcutActionRegistrar::end() const
{
    return this->bindings.end();
}