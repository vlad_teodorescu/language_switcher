/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef SHORTCUT_H
#define SHORTCUT_H
#pragma once

#include <functional>
#include <libconfig.h++>
#include <map>
#include <string>

#include "ShortcutGroupEntry.h"

using namespace std;

typedef function<void(void)> ShortcutAction;
typedef map<string, ShortcutAction> ShortcutActionMap;

class Shortcut : public ShortcutGroupEntry
{
private:
    Shortcut(const Shortcut &that);

    Shortcut &operator=(const Shortcut &that);

    const libconfig::Setting *node;
    ShortcutAction *action;
    string value_main;
    string value_alt;

protected:
    static inline const string NODE_MAIN = "main";
    static inline const string NODE_ALT = "alt";

public:
    /**
     * @brief Constructs a new shortcut object from scratch.
     *
     * @param node libconfig configuration node that holds shortcut configuration
     * @param parent parent to which this shortcut belongs
     */
    Shortcut(const libconfig::Setting &node, const ShortcutGroupEntry *parent);

    /**
     * @brief Constructs a new shortcut object based on an existing shortcut object.
     *
     * @param that shortcut object that serves as a blueprint
     * @param parent parent to which this shortcut belongs
     */
    Shortcut(const Shortcut &that, const ShortcutGroupEntry *parent);

    /**
     * @brief Checks if the given configuration node has the structure (respective properties in proper format) of a shortcut configuration node.
     *
     * @param node node to check
     * @return true if the node represents a shortcut configuration
     * @return false otherwise
     */
    static bool isShortcutConfigNode(const libconfig::Setting &node);

    /**
     * @brief Returns the shortcut's keyboard combination for the main keyboard binding.
     *
     * @return shortcut main binding
     */
    string getMain() const;

    /**
     * @brief Sets the shortcut's keyboard combination for the main keyboard binding.
     *
     * @param value new keyboard combination
     */
    void setMain(string value);

    /**
     * @brief Returns the shortcut's keyboard combination for the alternative keyboard binding.
     *
     * @return shortcut alternative binding
     */
    string getAlt() const;

    /**
     * @brief Sets the shortcut's keyboard combination for the alternative keyboard binding.
     *
     * @param value new keyboard combination
     */
    void setAlt(string value);

    /**
     * @brief Loads the values (main and alt) from the configuration node.
     */
    void load();

    /**
     * @brief Writes the values (main and alt) to the configuration node.
     */
    void save();

    /**
     * @brief Merges the values of the other shortcut into its own values.
     *
     * @param that other shortcut from which the values should be merged
     */
    void mergeFrom(const Shortcut &that);

    /**
     * @brief Binds the current shortcut combinations to the shortcut's assigned action.
     *
     * @param actions action map containing the mapping between shortcut IDs and the action that they should perform
     */
    void bind(const ShortcutActionMap &actions);

    /**
     * @brief Unbinds the current shortcut combinations from the shortcut's assigned action.
     *
     * @param actions action map containing the mapping between shortcut IDs and the action that they should perform
     */
    void unbind(const ShortcutActionMap &actions);

    friend bool operator<(const Shortcut &s1, const Shortcut &s2);

    friend bool operator>(const Shortcut &s1, const Shortcut &s2);
};

#endif // SHORTCUT_H
