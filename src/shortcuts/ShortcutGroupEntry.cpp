/**
 *   Copyright (C) 2022  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "ShortcutGroupEntry.h"

ShortcutGroupEntry::ShortcutGroupEntry(const string &code, const ShortcutGroupEntry *parent): code(code), parent(parent)
{

}

string ShortcutGroupEntry::getCode() const
{
	return this->code;
}

string ShortcutGroupEntry::getCanonicalCode() const
{
    // do not include the qualifier of the root group, because it is a dummy group
    if (this->parent == NULL)
        return "";
    
    string parent_qualifier = this->parent->getCanonicalCode();
    if (!parent_qualifier.length())
        return this->code;

    return parent_qualifier + "." + this->code;
}

