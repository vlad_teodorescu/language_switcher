/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include <exception>
#include <gtkmm/application.h>
#include <iostream>
#include <signal.h>
#include <spdlog/spdlog.h>
#include <string>

#include "application/MainApplication.h"
#include "i18n/i18n.h"
#include "monitoring/Watchdog.h"
#include "utils/spdlog.h"

using namespace std;

shared_ptr<Watchdog> watchdog;

// Define the function to be called when ctrl-c (SIGINT) signal is sent to process
void signal_callback_handler(int signum)
{
    switch (signum)
    {
    case SIGALRM:
        // MainApplication::allowXEvents();
        break;

    case SIGINT:
    case SIGHUP:
        spdlog::info("Signal {} received, time to exit", signum);

        exit(signum);
        break;
    }
}

/**
 * @brief Setups the signal handling routine.
 */
void setup_interrupt_handler()
{
    struct sigaction sact;

    sigemptyset(&sact.sa_mask);
    sact.sa_flags = 0;
    sact.sa_handler = signal_callback_handler;

    sigaction(SIGALRM, &sact, NULL);
    sigaction(SIGINT, &sact, NULL);
    sigaction(SIGHUP, &sact, NULL);
}

void init_locale_cfg()
{
#if defined ENABLE_NLS && ENABLE_NLS
    spdlog::info("Using locale {}", setlocale(LC_ALL, ""));
    spdlog::info("Binding text domain {} to {}", PACKAGE, PACKAGE_LOCALE_DIR);
    bindtextdomain(PACKAGE, PACKAGE_LOCALE_DIR);
    textdomain(PACKAGE);
    bind_textdomain_codeset(PACKAGE, PACKAGE_LOCALE_CHARSET);
#else
    spdlog::info("Not using NLS system, translations not available");
#endif
}

void setup_logger()
{
    try
    {
        auto console_sink = utils::spdlog::init_console_sink(spdlog::level::info);
        auto syslog_sink = utils::spdlog::init_syslog_sink(PACKAGE, spdlog::level::warn);

        spdlog::set_default_logger(utils::spdlog::init_multisink_logger({syslog_sink, console_sink}));
    }
    catch (const spdlog::spdlog_ex &e)
    {
        std::cerr << "Log initialization failed: " << e.what() << std::endl;
    }
}

int main(int argc, char *argv[])
{
    // Watches over the process space, kills already existing instance if needed
    watchdog = make_shared<Watchdog>(getpid());

    setup_logger();
    setup_interrupt_handler();
    init_locale_cfg();

    int result = 0;

    try
    {
        watchdog->ensureOnlyRunningInstance();

        Glib::RefPtr<Gtk::Application> gtk_app = Gtk::Application::create(argc, argv, PACKAGE_CANONICALNAME);
        MainApplication main_app(gtk_app);

        return main_app.run();
    }
    catch (const std::exception &e)
    {
        spdlog::error("Application threw exception, exiting. Message: {}", e.what());
        return -1;
    }
    catch (const std::string &e)
    {
        spdlog::error("Application threw exception, exiting: {}", e);
        return -2;
    }
    catch (const Glib::Exception &e)
    {
        spdlog::error("Application threw exception, exiting: {}", e.what().c_str());
        return -3;
    }
}