/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it
 *under the terms of the GNU Lesser General Public License as published by the
 *Free Software Foundation; either version 3 of the License, or (at your option)
 *any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT
 *ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 *details.
 *
 *   You should have received a copy of the GNU LGPL License along with this
 *library; if not, write to the Free Software Foundation, Inc., 51 Franklin
 *Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "MainApplication.h"

#include <memory>
#include <spdlog/spdlog.h>

#include "../config/ApplicationConfig.h"
#include "../exceptions/NoSuchElementException.h"
#include "../exceptions/SystemException.h"
#include "../i18n/i18n.h"
#include "../shortcuts/ShortcutRegistrar.h"
#include "../utils/path.h"

using namespace std;

bool MainApplication::x_events_allowed = false;

namespace 
{
    /** 
     * Helper function to ensure that the window gets hidden before it is drawn: connecting the hide() command to the show() signal would result
     * in the window flashing on screen for a fraction of a second, which is kind of ugly. The only downside to this is the ugly Gtk-CRITICAL message
     * that is printed to the console, everything else seems to work just fine.
     */
    void hide_before_show(Gtk::Allocation & allocation, shared_ptr<Gtk::ApplicationWindow> &window)
    {
        window->hide();
    }
}

MainApplication::MainApplication(Glib::RefPtr<Gtk::Application> &gtk_app): gtk_app(gtk_app)
{
    ShortcutRegistrar::initialize(ApplicationConfig::get().getShortcuts());
    ShortcutRegistrar::get().registerCallback(TOGGLE, std::bind(&MainApplication::cycleCurrentLayout, this));
    ShortcutRegistrar::get().bindAll();

    
    this->res_set = make_unique<LocalizedResourceSet>(this->getLayoutsSubdir());
    this->readAndSelectDefaultLayout();

    this->main_window = make_unique<Gtk::ApplicationWindow>();
    this->preferences_window = make_unique<PreferencesWindow>(ShortcutRegistrar::get(), ApplicationConfig::get(), *this->res_set);

    this->menu_builder = make_unique<MenuBuilder>(ACTION_GID);
    this->menu_builder->addResourceSetEntries(*this->res_set, sigc::mem_fun1(*this, &MainApplication::setLayout));
    this->menu_builder->addCustomEntry(_("menu.preferences"), Gtk::Stock::PREFERENCES,
                                       sigc::mem_fun(*this->preferences_window, &PreferencesWindow::show));
    this->menu_builder->addCustomEntry(_("menu.quit"), Gtk::Stock::QUIT, sigc::mem_fun(*this, &MainApplication::terminate));

    this->systray = make_unique<Systray>(this->getCurrentLayoutIconPath(), *this->menu_builder);
    this->systray->setTooltip(SYSTRAY_TOOLTIP);
    this->doAfterLayoutChange(true);

    gtk_app->hold();

    this->main_window->signal_size_allocate().connect(sigc::bind(&hide_before_show, this->main_window));
    this->main_window->show_all_children();
}

int MainApplication::run()
{
    return gtk_app->run(*this->main_window);
}

void MainApplication::terminate()
{
    gtk_app->release();
}

void MainApplication::execLayoutChangeScript()
{
    string command = SCRIPT_BIN + " " + res_set->getCurrentLayout().getScriptFilePath();

    int ret = system((char *)command.c_str());
    if (ret != 0)
        throw SystemException("Failed to execute layout change script");
}

void MainApplication::readAndSelectDefaultLayout()
{
    string layout = ApplicationConfig::get().getDefaultLayout();
    if (!layout.length())
    {
        this->res_set->select(0);
        return;
    }

    try
    {
        this->res_set->select(layout);
    }
    catch (const NoSuchElementException &e)
    {
        spdlog::warn("Could not select {} as default layout, falling back", layout);
        this->res_set->select(0);
    }
}

void MainApplication::doAfterLayoutChange(bool silent)
{
    spdlog::info("Current layout set to {}", this->res_set->getCurrentLayout().getCode());

    this->execLayoutChangeScript();

    this->systray->setIcon(this->getCurrentLayoutIconPath());

    // old actions will be overwritten
    menu_builder->populateActions(this->systray->getActionGroup(), this->res_set->getCurrentLayout().getCode());

    if (!silent)
        this->announceLayoutChange();
}

void MainApplication::announceLayoutChange()
{
    if (!ApplicationConfig::get().getShowNotificationLayoutChange())
        return;

    string layout_name = this->res_set->getCurrentLayout().getName();
    Glib::RefPtr<Gio::Notification> notif = Gio::Notification::create(_("language.change.title"));
    notif->set_body(Glib::ustring::compose(_("language.change.text"), layout_name));
    notif->set_icon(Gio::Icon::create(this->getCurrentLayoutIconPath()));

    gtk_app->send_notification("announce_layout_change", notif);
}

string MainApplication::getLayoutsSubdir() const
{
    return utils::path::merge({DATA_DIR, DATA_SUBDIR_LAYOUTS});
}

string MainApplication::getCurrentLayoutIconPath() const
{
    return this->res_set->getCurrentLayout().getIconPath();
}

void MainApplication::setLayout(const string &lang)
{
    // if no layout change needs to be done
    if (!this->res_set->select(lang))
        return;

    this->doAfterLayoutChange();
}

void MainApplication::cycleCurrentLayout()
{
    this->res_set->next();

    this->doAfterLayoutChange();
}

void MainApplication::disallowXEvents()
{
    MainApplication::x_events_allowed = false;
    spdlog::info("XEvents: Off");
}

void MainApplication::allowXEvents()
{
    MainApplication::x_events_allowed = true;
    spdlog::info("XEvents: On");
}

bool MainApplication::eventsAllowed()
{
    return MainApplication::x_events_allowed;
}

MainApplication::~MainApplication()
{
}