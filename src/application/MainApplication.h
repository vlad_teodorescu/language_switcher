/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef MAINAPPLICATION_H
#define MAINAPPLICATION_H
#pragma once

#include <gtkmm/application.h>
#include <gtkmm/applicationwindow.h>
#include <string>

#include "../gui/systray/MenuBuilder.h"
#include "../gui/systray/Systray.h"
#include "../gui/windows/PreferencesWindow.h"
#include "../i18n/i18n.h"
#include "../resources/LocalizedResourceSet.h"
#include "../shortcuts/ShortcutId.h"
#include "config.h"

using namespace std;

class MainApplication
{
private:
    MainApplication(const MainApplication &that) = delete;
    MainApplication &operator=(const MainApplication &that) = delete;

    Glib::RefPtr<Gtk::Application> &gtk_app;

    unique_ptr<MenuBuilder> menu_builder;
    shared_ptr<Gtk::ApplicationWindow> main_window;
    shared_ptr<PreferencesWindow> preferences_window;
    unique_ptr<Systray> systray;
    unique_ptr<LocalizedResourceSet> res_set; // Holds message sources for all available layouts, and can build paths to various resources

    // If false, x events are ignored. If true, they are processed
    static bool x_events_allowed;

protected:
    static inline const string DATA_DIR = PACKAGE_DATA_DIR;
    static inline const string SYSTRAY_TOOLTIP = PACKAGE_FULLNAME;
    static inline const string SCRIPT_BIN = "xmodmap";
    static inline const string DATA_SUBDIR_LAYOUTS = "layouts";
    static inline const string ACTION_GID = "systray";
    static inline const ShortcutId TOGGLE = ShortcutId::make("toggle", N_("shortcuts.toggle.name"), N_("shortcuts.toggle.description"));

    /**
     * @brief Reads the default layout from the application configuration and selects it. If the selection fails, the first available layout is
     * selected.
     */
    void readAndSelectDefaultLayout();

    /**
     * @brief Performs operations needed after a layout change. Function expects the correct
     * layout to have already been selected
     * @param silent if true, the layout change will not be announced to the user (tooltip)
     */
    void doAfterLayoutChange(bool silent = false);

    /**
     * @brief Announces that the layout has been changed by displaying a tooltip
     */
    void announceLayoutChange();

    /**
     * @brief Executes xmodmap, activating the layout for the layout currently selected
     */
    void execLayoutChangeScript();

    /**
     * @brief Returns the data subfolder containing layout data
     * @return subfolder containing layout data
     */
    string getLayoutsSubdir() const;

    /**
     * @brief Returns the path of the current layout icon.
     *
     * @return system path to layout icon
     */
    string getCurrentLayoutIconPath() const;

public:
    /**
     * @brief Default contructor.
     */
    MainApplication(Glib::RefPtr<Gtk::Application> &gtk_app);

    /**
     * @brief cycles through the layouts, setting the next one as the current layout. At the end of the list, the
     * function starts with the beginning.
     */
    void cycleCurrentLayout();

    /**
     * @brief Sets the current layout based on the layout's index.
     *
     * @param lang language code of layout to set
     */
    void setLayout(const string &lang);

    /**
     * @brief Executes the application.
     *
     * @return exit status
     */
    int run();

    /**
     * @brief Terminates the application.
     */
    void terminate();

    /**
     * @brief Makes the application not listen to X events.
     */
    static void disallowXEvents();

    /**
     * @brief Makes the application listen to X events.
     */
    static void allowXEvents();

    /**
     * @brief Checks if the application is allowed to listen to X events.
     *
     * @return true if the application is allowed to listen to X events, false otherwise
     */
    static bool eventsAllowed();

    /**
     * @brief Virtual destructor
     */
    virtual ~MainApplication();
};

#endif // MAINAPPLICATION_H
