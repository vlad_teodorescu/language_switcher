/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef APPLICATIONCONFIG_H
#define APPLICATIONCONFIG_H
#pragma once

#include "config.h"
#include <libconfig.h++>
#include <memory>

#include "../shortcuts/ShortcutGroup.h"
#include "../utils/file.h"
#include "../utils/path.h"

using namespace std;

class ApplicationConfig
{

private:
    ApplicationConfig(const ApplicationConfig &that);

    ApplicationConfig &operator=(const ApplicationConfig &that);

protected:
    static inline const string CFG_DIR = PACKAGE_CONFIG_DIR;
    static inline const string DATA_DIR = PACKAGE_DATA_DIR;
    static inline const string CFG_SUBDIR = (string)PACKAGE;
    static inline const string CFG_FILE_NAME = "application.conf";
    static inline const string CFG_NODE_SHORTCUTS = "application.shortcuts";
    static inline const string CFG_NODE_ROOT = "application";
    static inline const string CFG_NODE_DEFAULT_LAYOUT = "default_layout";
    static inline const string CFG_NODE_NOTIF_LAYOUT_CHANGE = "show_notification_layout_change";

    unique_ptr<libconfig::Config> current_config;
    unique_ptr<ShortcutGroup> shortcuts;

    /**
     * @brief Construct a new ApplicationConfig object.
     */
    ApplicationConfig();

    /**
     * @brief Ensures that the configuration file that is used to construct the in-memory config object is valid.
     *
     * * @throws ConfigurationException if the class was unable to bring the file to the proper condition
     */
    void ensureValidConfigFile();

    /**
     * @brief Returns the path to the folder containing the application configuration.
     *
     * @return path to app config folder
     */
    string getConfigDirPath();

    /**
     * @brief Returns the path to the file containing the application configuration.
     *
     * @return path to app config file
     */
    string getConfigFilePath();

    /**
     * @brief Returns the path to the file containing the template for the application configuration(standard/default configuration).
     *
     * @return path to app config template
     */
    string getTemplateConfigFilePath();

    /**
     * @brief Initializes the configuration file from the configuration template file.
     */
    void initializeConfigFromTemplate();

    /**
     * @brief Ensures that the configuration template is valid.
     *
     * @throws ConfigurationException if file is invalid
     */
    void ensureValidConfigTemplate();

    /**
     * @brief Ensures that the configuration folder is valid.
     *
     * @throws ConfigurationException if the class was unable to bring the folder to the proper condition
     */
    void ensureValidConfigFolder();

    /**
     * @brief Given a path relative to the root node, compiles the absolute path (including the root node). The path is expressed in parts, each part
     * representing a level in the hierarchy.
     * 
     * @param relative_path path relative to root configuration node
     * @return absolute path
     */
    string absPath(const vector<string> &relative_path);

    /**
     * @brief Ensures that the relative path exists and its leaf node is of the given type. If one or more of the parts are missing, they are created
     * as groups (except for the leaf, which is created as the given type). If one or more of the parts are of the wrong type, they are deleted and
     * recreated.
     * 
     * @param relative_path path relative to root configuration node
     * @param type type of configuration node to check/create
     */
    void ensureExists(const vector<string> &relative_path, const libconfig::Setting::Type &type);

    /**
     * @brief Looks up the path relative to the root node
     * 
     * @param relative_path path to lookup
     * @return setting node
     * @throws SettingNotFoundException if the setting node was not found
     */
    libconfig::Setting & lookup(const vector<string> &relative_path);

public:
    /**
     * @brief Returns a reference to a singleton instance.
     *
     * @return singleton instance
     */
    static ApplicationConfig &get();

    /**
     * @brief Returns the configured value for the default layout.
     * 
     * @return default layout, or empty string if none configured 
     */
    string getDefaultLayout();

    /**
     * @brief Sets the default layout.
     * 
     * @param code isocode of layout to be set as a default layout
     */
    void setDefaultLayout(string code);

    /**
     * @brief Returns the configured value for the "Show notification on layout change" property.
     * 
     * @return configured boolean value
     */
    bool getShowNotificationLayoutChange();

    /**
     * @brief Sets the configured value for the "Show notification on layout change" property.
     * 
     * @param value value to set
     */
    void setShowNotificationLayoutChange(bool value);

    /**
     * @brief Returns the top-level configuration group for the shortcut configuration
     *
     * @return shortcuts group
     */
    ShortcutGroup &getShortcuts();

    /**
     * @brief Writes the current configuration values to the configuration file.
     */
    void save();
};

#endif // APPLICATIONCONFIG_H
