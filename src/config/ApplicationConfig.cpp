/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "ApplicationConfig.h"

#include <filesystem>
#include <fstream>
#include <libconfig.h++>
#include <spdlog/spdlog.h>

#include "../exceptions/ConfigurationException.h"
#include "../utils/str.h"

using namespace std;

ApplicationConfig::ApplicationConfig()
{
    this->ensureValidConfigFile();
    this->current_config = make_unique<libconfig::Config>();

    try
    {
        this->current_config->readFile(this->getConfigFilePath().c_str());
    }
    catch (libconfig::ParseException &e)
    {
        spdlog::warn("Error while parsing configuration file {}, line {}: {}", e.getFile(), e.getLine(), e.getError());
        this->initializeConfigFromTemplate();
        this->current_config->readFile(this->getConfigFilePath().c_str());
    }

    this->shortcuts = make_unique<ShortcutGroup>(this->current_config->lookup(CFG_NODE_SHORTCUTS), nullptr);
}

void ApplicationConfig::ensureValidConfigFile()
{
    string fname = this->getConfigFilePath();

    if (utils::path::is_path_free(fname))
    {
        this->initializeConfigFromTemplate();
    }
    else
    {
        if (!utils::path::is_path_readable(fname))
        {
            throw ConfigurationException("Configuration file " + fname + " may exist, but cannot be read");
        }

        if (!utils::path::is_path_file(fname))
        {
            if (!utils::file::purge_path(fname))
            {
                throw ConfigurationException("Could not clean up path " + fname);
            }

            this->initializeConfigFromTemplate();
        }
    }
}

string ApplicationConfig::getConfigDirPath()
{
    string ret = utils::path::merge({CFG_DIR, CFG_SUBDIR});

    return utils::path::expand_path(ret);
}

string ApplicationConfig::getConfigFilePath()
{
    return utils::path::merge({getConfigDirPath(), CFG_FILE_NAME});
}

string ApplicationConfig::getTemplateConfigFilePath()
{
    return utils::path::merge({DATA_DIR, CFG_FILE_NAME});
}

void ApplicationConfig::initializeConfigFromTemplate()
{
    this->ensureValidConfigTemplate();
    this->ensureValidConfigFolder();

    // copy file from data folder to config folder
    string source = this->getTemplateConfigFilePath();
    string target = this->getConfigFilePath();

    std::ifstream src(source);
    std::ofstream dst(target, std::ios::trunc);
    dst << src.rdbuf();

    if (utils::path::is_path_free(target))
    {
        throw ConfigurationException("Configuration file " + target + " cannot be created");
    }
}

void ApplicationConfig::ensureValidConfigTemplate()
{
    string tmplt = this->getTemplateConfigFilePath();

    if (utils::path::is_path_free(tmplt))
    {
        throw ConfigurationException("Missing template configuration file " + tmplt);
    }

    if (!utils::path::is_path_readable(tmplt))
    {
        throw ConfigurationException("Path " + tmplt + " exists but is not readable");
    }

    if (!utils::path::is_path_file(tmplt))
    {
        throw ConfigurationException("Path " + tmplt + " exists but is not a file");
    }
}

void ApplicationConfig::ensureValidConfigFolder()
{
    string cfgdir = this->getConfigDirPath();
    if (utils::path::is_path_free(cfgdir))
    {
        if (!utils::file::make_dir_recursive(cfgdir))
        {
            throw ConfigurationException("Could not make configuration folder " + cfgdir);
        }
    }

    if (!utils::path::is_path_folder(cfgdir))
    {
        throw ConfigurationException("Path " + cfgdir + " exists, but is not a folder");
    }
}

ApplicationConfig &ApplicationConfig::get()
{
    // The only instance
    // Guaranteed to be lazy initialized
    // Guaranteed that it will be destroyed correctly
    static ApplicationConfig instance;

    return instance;
}

string ApplicationConfig::absPath(const vector<string> &relative_path)
{
    string relative_path_str = utils::str::join(relative_path, ".");
    return CFG_NODE_ROOT + "." + relative_path_str;
}

void ApplicationConfig::ensureExists(const vector<string> &relative_path, const libconfig::Setting::Type &type)
{
    if (relative_path.empty())
        return;
    
    string abs_path = this->absPath(relative_path);
    if (this->current_config->exists(abs_path))
    {
        libconfig::Setting &setting = this->current_config->lookup(abs_path);
        if (setting.getType() == type)
            return;

        string setting_name = setting.getName();
        setting.getParent().remove(setting_name);
    }
    
    libconfig::Setting *node = &(this->current_config->lookup(CFG_NODE_ROOT));
    int num_parts = relative_path.size();
    for (int i = 0; i < num_parts; i++)
    {
        string part_name = relative_path[i];
        const libconfig::Setting::Type &part_type = (i == num_parts - 1) ? type : libconfig::Setting::TypeGroup;

        if (node->exists(part_name))
        {
            if (node->lookup(part_name).getType() != part_type)
            {
                node->remove(part_name);
                node->add(part_name, part_type);
            }
        }
        else
            node->add(part_name, part_type);

        node = &(node->lookup(part_name));
    }
}

libconfig::Setting &ApplicationConfig::lookup(const vector<string> &relative_path)
{
    string relative_path_str = utils::str::join(relative_path, ".");
    string abs_path_str = CFG_NODE_ROOT + "." + relative_path_str;

    return this->current_config->lookup(this->absPath(relative_path));
}

string ApplicationConfig::getDefaultLayout()
{
    string result;
    this->current_config->lookupValue(this->absPath({CFG_NODE_DEFAULT_LAYOUT}), result);
    return result;
}

void ApplicationConfig::setDefaultLayout(string code)
{
    this->ensureExists({CFG_NODE_DEFAULT_LAYOUT}, libconfig::Setting::Type::TypeString);
    this->lookup({CFG_NODE_DEFAULT_LAYOUT}) = code;
}

bool ApplicationConfig::getShowNotificationLayoutChange()
{
    bool result = false;
    this->current_config->lookupValue(this->absPath({CFG_NODE_NOTIF_LAYOUT_CHANGE}), result);
    return result;
}

void ApplicationConfig::setShowNotificationLayoutChange(bool value)
{
    this->ensureExists({CFG_NODE_NOTIF_LAYOUT_CHANGE}, libconfig::Setting::Type::TypeBoolean);
    this->lookup({CFG_NODE_NOTIF_LAYOUT_CHANGE}) = value;
}

ShortcutGroup &ApplicationConfig::getShortcuts()
{
    return *this->shortcuts;
}

void ApplicationConfig::save()
{
    this->current_config->writeFile(this->getConfigFilePath());
}