#ifndef WATCHDOG_H
#define WATCHDOG_H

#include <string>
#include <sys/types.h>
/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "config.h"
#include "../utils/path.h"

using namespace std;

class Watchdog
{

private:
    pid_t current_pid;

protected:
    static inline const unsigned int PID_NUM_DIGITS = 5;

    static inline const string PROC_PATH_PFX = utils::path::PATH_SEPARATOR + "proc";
    static inline const string CMDLINE_PATH_SFX = "cmdline";
    static inline const string PID_FILE_DIR = PACKAGE_TMP_DIR;
    static inline const string PID_FILE_NAME = (string)PACKAGE + ".pid";

    /**
     * @brief Reads the PID stored in the PID file
     * @return stored PID
     */
    pid_t readPidFromFile();

    /**
     * @brief Returns the full path to the PID file
     * @return PID file path
     */
    string getPidFilePath();

    /**
     * @brief Checks if the given PID is still running
     * @param pid PID to check
     * @return true if the process with the given ID is still running, false otherwise
     */
    bool isInstanceRunning(pid_t pid);

    /**
     * @brief Returns the FS file path of the cmdline file (/proc/PID/cmdline)
     * @param pid PID
     * @return path of cmdline file
     */
    string getCmdlineFilePath(pid_t pid);

    /**
     * @brief Kills a process instance
     * @param pid PID to kill
     */
    void killInstance(pid_t pid);

    /**
     * @brief Writes the PID of the current process to file
     */
    void writeCurrentPidToFile();

    /**
     * @brief Removes the PID file
     */
    void removePidFile();

public:
    Watchdog(pid_t current_pid);

    /**
     * @brief Ensures that the current instance is the only one running.
     */
    void ensureOnlyRunningInstance();

    ~Watchdog();
};

#endif // WATCHDOG_H
