/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "Watchdog.h"

#include <fstream>
#include <iostream>
#include <signal.h>
#include <spdlog/spdlog.h>
#include <sys/types.h>

#include "../utils/common.h"
#include "../utils/path.h"
#include "../exceptions/IoException.h"

using namespace std;

inline void sanitize_pid_string(char *pid_str, int size)
{
    for (int i = 0; i < size; i++)
    {
        if (!utils::common::is_digit(pid_str[i]))
        {
            // beginning at the first non-digit, remove all characters
            fill_n(pid_str + i, size - i, 0);
            return;
        }
    }
}

Watchdog::Watchdog(pid_t pid) : current_pid{pid}
{
}

void Watchdog::ensureOnlyRunningInstance()
{
    pid_t old_pid = this->readPidFromFile();

    if (this->isInstanceRunning(old_pid))
    {
        spdlog::info("Found running instance with PID {}, killing", old_pid);
        this->killInstance(old_pid);
    }

    this->writeCurrentPidToFile();
}

pid_t Watchdog::readPidFromFile()
{
    string fname = this->getPidFilePath();

	if (utils::path::is_path_free(fname))
	{
		return 0;
	}

	if (!utils::path::is_path_readable(fname))
	{
		throw IoException("Cannot read path " + fname);
	}

	if (!utils::path::is_path_file(fname))
	{
		throw IoException("Expected " + fname + " to be a file, but it's not");
	}
	
	fstream f(fname.c_str(), ios::in);
	if (!f.is_open())
	{
		throw IoException("Failed to open PID file " + fname);
	}
	
	// Read the old PID(if exists), to see if the process still exists
	pid_t file_pid;
    char s_file_pid[PID_NUM_DIGITS];
    fill_n(s_file_pid, PID_NUM_DIGITS, 0);

    f.read(s_file_pid, PID_NUM_DIGITS);
    f.close();

    sanitize_pid_string(s_file_pid, PID_NUM_DIGITS);
    file_pid = atoi(s_file_pid);

    return file_pid;
}

string Watchdog::getPidFilePath()
{
    return utils::path::merge({PID_FILE_DIR, PID_FILE_NAME});
}

bool Watchdog::isInstanceRunning(pid_t pid)
{
    if (pid < 1)
    {
        return false;
    }

    char s_cmd_name[1024];
    fill_n(s_cmd_name, 1024, 0);

    // Check the system's respective cmdline file for the process name

    string proc_fpath = this->getCmdlineFilePath(pid);

    ifstream f_pid(proc_fpath.c_str());
    if (!f_pid.good())
    {
        // there is no process associated with given PID
        return false;
    }

    if (!f_pid.is_open())
    {
        throw IoException("Could not open process file " + proc_fpath);
    }

    f_pid.read(s_cmd_name, 1024);
    f_pid.close();

    string aux(s_cmd_name);

    // is it our process?
    return aux.find(PACKAGE) != string::npos;
}

string Watchdog::getCmdlineFilePath(pid_t pid)
{
    return utils::path::merge({PROC_PATH_PFX, to_string(pid), CMDLINE_PATH_SFX});
}

void Watchdog::killInstance(pid_t pid)
{
    if (pid < 1)
    {
        return;
    }

    if (kill(pid, SIGINT) != 0)
    {
        if (kill(pid, SIGKILL) != 0)
        {
            throw SystemException("Failed to kill old process " + to_string(pid));
        }
    }
}

void Watchdog::writeCurrentPidToFile()
{
    fstream f(this->getPidFilePath().c_str(), ios::out);
    f << current_pid;
    f.close();
}

Watchdog::~Watchdog()
{
	if (!utils::path::is_path_free(this->getPidFilePath()))
	{
		try
		{
			this->removePidFile();
		}
		catch (const IoException& e)
		{
			spdlog::error(e.what());
		}
	}
}

void Watchdog::removePidFile()
{
    if (remove(this->getPidFilePath().c_str()) != 0)
    {
        throw IoException("Could not remove PID file " + this->getPidFilePath());
    }
}
