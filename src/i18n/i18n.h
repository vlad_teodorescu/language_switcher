/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms
 *   of the GNU Lesser General Public License as published by the Free Software Foundation;
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#ifndef I18N_H
#define I18N_H

#include <locale.h>

#include "config.h"
#include "gettext.h"

/*
    The macro _() serves a double purpose:
    1. it translates strings at runtime and
    2. it marks strings as translatable when gettext is invoked for string translation inside of the build system.

    In some cases, it is not possible to do both operations at the same time. Static variables, for example, are initialized before gettext
	is, so it is not possible to have their values translated with _(). As such, a second operator is defined, N_(), which only extracts the
	text to the .po file, but does not wrap it in a gettext() call. The string remains untranslated, and has to be translated later, when it is
	used.
*/

// Only extracts the current string to the .po files. The string is not translated at runtime
#define N_(String) String

#if defined ENABLE_NLS && ENABLE_NLS
// Performs both the extraction (to the .po files) and runtime translation of the given string
#define _(String) gettext(String)
#else
#define _(String) String
#endif

#endif
