/**
 *   Copyright (C) 2023  Vlad Teodorescu
 *
 *   This library is free software; you can redistribute it and/or modify it under the terms 
 *   of the GNU Lesser General Public License as published by the Free Software Foundation; 
 *   either version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 *   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *   See the GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU LGPL License along with this library; if not, write to 
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

#include "SystemException.h"

SystemException::SystemException(const string message)
{
	this->message = message;
}
SystemException::SystemException(const string message, SystemException const* cause): SystemException(message)
{
	this->cause = cause;
}

const char* SystemException::what() const throw()
{
	return this->message.c_str();
}

SystemException::~SystemException() throw()
{

}

