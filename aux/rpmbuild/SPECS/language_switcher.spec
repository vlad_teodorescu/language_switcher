#
# spec file for package language_switcher
#
# Copyright (c) 2023 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#


Name:           language_switcher
Summary:        Language Switcher
Version:        1.6
Release:        1
License:        GNU LGPL 3        
URL:            https://vladteodorescu.ro
Source:         %{name}-%{version}.tar.gz
BuildRequires:  make bash


%description
Applet which allows the user to quickly switch between various keyboard layouts using various XModMap configuration files. Each such layout must have a flag, a translations file and a xmodmaprc file from which xmodmap will be configured.

%prep
%autosetup

%build
%configure CXXFLAGS="-O2"
%make_build

%install
%make_install

%clean
rm -rf $RPM_BUILD_ROOT

%post
%postun

%files
/usr/bin/language_switcher
/usr/share/doc/language_switcher/LICENSE
/usr/share/doc/language_switcher/README
/usr/share/language_switcher/application.conf
/usr/share/language_switcher/preferences_window.glade
/usr/share/language_switcher/layouts/de/flag.png
/usr/share/language_switcher/layouts/de/layout.conf
/usr/share/language_switcher/layouts/de/xmodmaprc
/usr/share/language_switcher/layouts/es/flag.png
/usr/share/language_switcher/layouts/es/layout.conf
/usr/share/language_switcher/layouts/es/xmodmaprc
/usr/share/language_switcher/layouts/ro/flag.png
/usr/share/language_switcher/layouts/ro/layout.conf
/usr/share/language_switcher/layouts/ro/xmodmaprc
/usr/share/locale/de/LC_MESSAGES/language_switcher.mo
/usr/share/locale/en/LC_MESSAGES/language_switcher.mo
/usr/share/locale/ro/LC_MESSAGES/language_switcher.mo

%changelog
