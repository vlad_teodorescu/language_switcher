#!/bin/sh

if [ -z "$1" ]; then
    SRC_DIR="./src"
else
    SRC_DIR=$1
fi

if [ -z "$2" ]; then
    POTFILES="POTFILES.in"
else
    POTFILES=$1
fi

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")

# Make sure this is executed in the top folder of the project, since the paths in
# the $POTFILES file have to be relative to that. Also, for consistency reasons
# (so that it becomes irellevant from which folder the script gets called)
cd $SCRIPTPATH/..

find $SRC_DIR -name "*.c" -o -name "*.cpp" -o -name "*.h" | sort > $SCRIPTPATH/$POTFILES

echo "$POTFILES refreshed, now at "`wc -l $SCRIPTPATH/$POTFILES | cut -d " " -f 1`" entries"
